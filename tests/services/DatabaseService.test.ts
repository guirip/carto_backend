import { connect, disconnect, ping } from '../../src/services/DatabaseService';

describe('DatabaseService', function () {
  describe('ping', function () {
    it('should properly indicate when DB connection is KO', async function () {
      const result = await ping();
      expect(result.isConnectionOk).toBe(false);
    });

    it('should properly indicate when DB connection is OK', async function () {
      await connect(null, true);
      const result = await ping();
      await disconnect();

      expect(result.isConnectionOk).toBe(true);
    });
  });
});
