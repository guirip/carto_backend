import mongoose from 'mongoose';

const schema = mongoose.Schema({
  SITE_ID: mongoose.Schema.Types.ObjectId,
  FILENAME: String,
  IMPORT_DATE: Date,
  IMPORT_AUTHOR: String,
  COMMENTAIRE: String,
});

export default mongoose.model('Photo', schema);
