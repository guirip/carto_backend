import mongoose from 'mongoose';

const schema = mongoose.Schema({
  COMMUNE: String,
  CP: { type: Number, index: true },
  INSEE: { type: Number, index: true },
});

export default mongoose.model('Commune', schema);
