import mongoose from 'mongoose';

const schema = mongoose.Schema({
  OLD_ID: Number,
  CREATION_DATE: Date,
  CREATION_AUTHOR: String,
  IDENTIFIANT: String,
  CODE_INSEE: { type: Number, index: true },
  DEPARTEMENT: String,
  PAYS: String,
  RAISON_SOCIALE: String,
  NOM_USUEL: String,
  COMMENTAIRE: String,
  ADRESSE: String,
  COMMENTAIRE_LOCALISATION: String,
  ETAT_DE_CONNAISSANCE: String,
  ETAT_OCCUPATION: { type: String, index: true },
  IS_ACTIVITE_TERMINEE: { type: Number, index: true, default: null },
  CODE_ACTIVITE: String,
  LIBELLE_ACTIVITE: String,
  COMMENTAIRE_ACTIVITE: String,
  X_LAMBERT_2E: Number,
  Y_LAMBERT_2E: Number,
  X_LAMBERT_93: Number,
  Y_LAMBERT_93: Number,
  PRECISION_XY: String,
  X_ADRESSE_LAMBERT_2E: Number,
  Y_ADRESSE_LAMBERT_2E: Number,
  PRECISION_ADRESSE: String,
  SURFACE_TOTALE: Number,
  SITE_REAMENAGE: { type: Number, index: true, default: null },
  SITE_EN_FRICHE: { type: Number, index: true, default: null },
  TYPE_DE_REAMENAGEMENT: String,
  PROJET_DE_REAMENAGEMENT: String,
  X_WGS_84: Number, // TODO: remove later
  Y_WGS_84: Number, // TODO: remove later
  GEO_POINT: {
    type: { type: String, default: 'Point' },
    coordinates: [Number],
  },
  POSITION_AUTHOR: String,
  POSITION_DATE: Date,
  ETAT: { type: Number, index: true },
  ETAT_AUTHOR: String,
  ETAT_DATE: Date,
  communeId: { type: mongoose.Schema.Types.ObjectId, ref: 'Commune' },
});

// schema.index({ GEO_POINT: '2dsphere' })

schema.methods.getLabel = function () {
  return this.NOM_USUEL || this.RAISON_SOCIALE;
};

export default mongoose.model('Site', schema);
