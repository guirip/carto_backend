import mongoose from 'mongoose';

const schema = mongoose.Schema({
  SITE_ID: mongoose.Schema.Types.ObjectId,
  AUTHOR: String,
  DATE: Date,
  REMARQUE: String,
  site: { type: mongoose.Schema.Types.ObjectId, ref: 'Site' },
});

export default mongoose.model('Remarque', schema);
