import mongoose from 'mongoose';

const schema = mongoose.Schema({
  id: Number,
  libelle: String,
  order: Number,
});

export default mongoose.model('Etat', schema);
