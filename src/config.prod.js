const DOMAIN = 'carto.heytonton.fr';

export default {
  appRoot: '',
  port: 3010,
  corsAllowedOrigins: [`https://${DOMAIN}`],

  sessionDurationInHours: 96,

  useHttps: true,
  keyPath: `/opt/node/certs/${DOMAIN}/privkey.pem`,
  certPath: `/opt/node/certs/${DOMAIN}/fullchain.pem`,

  redis: {
    host: 'localhost',
    port: 6379,
  },

  db: {
    host: '127.0.0.1',
    port: 27017,
    instance: 'carto',
    user: 'carto',
    passwd: 'passmo2!carto',
  },

  webSockets: {
    port: 3001,
  },
};
