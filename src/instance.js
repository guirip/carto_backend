// Express server
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import cors from 'cors';
import express from 'express';
import compression from 'compression';
import session from 'express-session';
import dotenv from 'dotenv';

import config from './helpers/config.js';
import * as DbService from './services/DatabaseService';
import * as Utils from './helpers/Utils';

import * as DiagRoutes from './routes/DiagRoutes';
import * as DiaryRoutes from './routes/DiaryRoutes';
import * as EtatRoutes from './routes/EtatRoutes';
import * as LoginRoutes from './routes/LoginRoutes';
// import * as PhotoRoutes from './routes/PhotoRoutes';
import * as PingRoutes from './routes/PingRoutes';
import * as RemarqueRoutes from './routes/RemarqueRoutes';
import * as SiteRoutes from './routes/SiteRoutes';
import * as RedisService from './services/RedisService';

const app = express();
const redisStore = RedisService.init();

let server,
  shuttingDown = false;

export async function start({ key, cert }) {
  dotenv.config();
  if (!process.env.COOKIE_KEY) {
    Utils.logError('Missing env var COOKIE_KEY');
    process.exit(1);
  }

  if (config.useHttps) {
    const options = { key, cert };
    const https = await import('https');
    server = https.createServer(options, app);
  } else {
    const http = await import('http');
    server = http.Server(app);
  }

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(cookieParser());
  app.use(compression());
  app.use(logger('combined'));
  app.disable('x-powered-by');
  app.use(
    session({
      saveUninitialized: false,
      cookie: {
        path: '/',
        maxAge: config.sessionDurationInHours * 3600 * 1000,
      },
      resave: true,
      rolling: true,
      store: redisStore,
      secret: process.env.COOKIE_KEY,
    })
  );

  app.use(
    cors({
      origin: config.corsAllowedOrigins,
      credentials: true,
      optionsSuccessStatus: 200,
    })
  );

  // Support preflight request to support CORS
  app.options('/*', function (_req, res) {
    res.sendStatus(200);
  });

  // Set encoding for all response
  app.all(config.appRoot + '*', function (_req, res, next) {
    res.charset = 'utf-8';
    res.append('Access-Control-Max-Age', '86400');
    next();
  });

  // Add routers to this array
  const allRoutes = [
    DiagRoutes,
    DiaryRoutes,
    EtatRoutes,
    LoginRoutes,
    // PhotoRoutes,
    PingRoutes,
    RemarqueRoutes,
    SiteRoutes,
  ];
  for (let routes of allRoutes) {
    const routePath = `${config.appRoot}${routes.root}`;
    Utils.logInfo(`Enabling router for ${routePath}`);
    app.use(routePath, routes.getRouter());
  }

  // If no route matched, return code 404
  app.all('*', function (req, res) {
    res.status(404).end();
  });

  // Error handler
  app.use(function errorHandler(err, req, res, next) {
    if (res.headersSent) {
      return next(err);
    }
    if (!err.httpCode || err.httpCode === 500) {
      Utils.log(
        '===== ERROR ===== ' +
          (err.name ? err.name + ' # ' : err) +
          (err.message ? err.message + ' # ' : err) +
          (err.stack ? err.stack : err)
      );
      if (err.errors) {
        // mongoose errors
        for (const field of Object.keys(err.errors)) {
          const error = err.errors[field];
          Utils.log(`${error.path} ${error.name}: ${error.message} # value: ${error.value}`);
        }
      }
    }
    res.status(err.httpCode ? err.httpCode : 500).end(err.message ? err.message : '');
  });

  // Open mongoose connection
  await DbService.connect();

  /**
   * Start server
   */
  function boot() {
    server.listen(config.port, function () {
      Utils.logInfo(
        `${config.useHttps ? 'HTTPS' : 'HTTP'} server is running and listening on port ${
          config.port
        }`
      );
    });
  }

  boot();
}

/**
 * Shutdown server
 */
export async function stop(code) {
  if (shuttingDown) return;
  shuttingDown = true;

  Utils.log('[instance] HTTP server shutting down');

  await DbService.disconnect();

  const exit = () => {
    const DELAY = 2000;
    Utils.log(`[instance] Process will exit in ${DELAY / 1000} sec...`);
    setTimeout(() => {
      process.exit(code ? 1 : 0);
    }, DELAY);
  };

  server &&
    server.close(() => {
      Utils.logInfo('[instance] HTTP server closed');
    });
  exit();
}

process.on('exit', stop);
process.on('SIGINT', stop);
