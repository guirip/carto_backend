import fs from 'fs';
import cluster from 'cluster';
import os from 'os';
import debounce from 'lodash.debounce';

import config from './helpers/config.js';
import * as Utils from './helpers/Utils';
import * as wsServer from './websockets-server';
import * as server from './instance';

const numCPUs = os.cpus().length;
const isDebug = process.env.debug && process.env.debug == 'true';

let clustering = !isDebug,
  key,
  cert,
  alreadyStopping = false;

if (config.useHttps) {
  try {
    key = fs.readFileSync(config.keyPath);
    cert = fs.readFileSync(config.certPath);
  } catch (error) {
    Utils.logError(
      'Failed to read certificate files. ' +
        (error ? `${error.code || ''} - ${error.name || ''} - ${error.message || ''}` : '')
    );
    process.exit(1);
  }
}

if (!clustering || cluster.isPrimary) {
  if (!process.env.NODE_ENV) {
    Utils.logError("Missing environment variable 'NODE_ENV' (e.g NODE_ENV=development)");
    process.exit(1);
  }
  Utils.logInfo('Starting node backend - environment ' + process.env.NODE_ENV);

  // Start single-threaded websocket server
  wsServer.start({ key, cert });
}

if (clustering && cluster.isPrimary) {
  Utils.log('Fork ' + numCPUs + ' worker(s) from master');

  for (let i = 0; i < numCPUs; i++) {
    const worker = cluster.fork();

    // Communication between cluster and master
    worker.on('message', function (msg) {
      if (msg.action === 'emit-ws-event') {
        wsServer.emit(msg.eventName, msg.eventData, msg.filter);
      } else {
        //Utils.error(`Unhandled message from worker (${this.pid}): `, msg)
      }
    });
  }

  cluster.on('disconnect', function (worker) {
    Utils.log('disconnected from the cluster.');
  });

  cluster.on('exit', function (worker, code, signal) {
    Utils.log(`died with exit code ${code} (${signal ?? 'no signal'})'}`);
  });
} else if (!clustering || cluster.isWorker) {
  server.start({ key, cert });
} else {
  Utils.logError('Startup failed');
}

/**
 * Handle shutdown
 * @param e
 * @private
 *
async function exitHandler() {
  console.log('start - exit');
  if (alreadyStopping) {
    return;
  }
  alreadyStopping = true;

  // wsServer.stop();

  if (server) {
    // await server.stop();
  }
}

process.on('exit', exitHandler);
process.on('SIGINT', exitHandler);
*/

process.on('uncaughtException', (e) => {
  Utils.logError(`Uncaught exception:`, e, e.stack);
});
