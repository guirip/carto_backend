export default {
  appRoot: '',
  port: 3010,

  redis: {
    host: 'localhost',
    port: 6379,
  },

  // dirPhotos: '/var/www/carto-static/photos',

  db: {
    host: '127.0.0.1',
    port: 27017,
    instance: 'carto-test',
    user: 'carto',
    passwd: 'passmo2!carto',
  },

  webSockets: {
    port: 3001,
  },
};
