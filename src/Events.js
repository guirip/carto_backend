export default {
  /**
   * SEE carto-client /src/js/modules/Events
   */
  SITE_CREATED: 'S_CR',
  SITE_POSITION_UPDATED: 'S_PU',
  SITE_ETAT_UPDATED: 'S_EU',
  REMARK_ADDED: 'R_CR',
  PHOTO_ADDED: 'P_AD',
};
