import cluster from 'cluster';
import express from 'express';
import socketIO from 'socket.io';

import config from './helpers/config';
import * as Utils from './helpers/Utils';
import * as RemarqueService from './services/RemarqueService';
import * as SiteService from './services/SiteService';
import Events from './Events';

let httpServer,
  ioServer,
  shuttingDown = false;
const sockets = {};

const getClientsCount = () => Object.keys(sockets).length;

/**
 * Start websocket server
 */
export async function start({ key, cert }) {
  if (config.webSockets.enabled !== true) {
    Utils.log('[WS] feature disabled');
    return;
  }

  const app = express();

  // Use http or https
  if (config.useHttps) {
    const options = { key, cert };
    const https = await import('https');
    httpServer = https.createServer(options, app);
  } else {
    const http = await import('http');
    httpServer = http.Server(app);
  }

  ioServer = socketIO(httpServer, { cors: { origin: '*' } });

  ioServer.on('connection', function (socket) {
    sockets[socket.id] = socket;
    Utils.log(`[WS] Client connected [${socket.id}] - Total clients: ${getClientsCount()}`);

    socket.on('disconnect', function (reason) {
      delete sockets[socket.id];
      Utils.log(
        `[WS] Client disconnected [${socket.id}]: ${reason} - Total clients: ${getClientsCount()}`
      );
    });

    socket.on('event', function (msg) {
      emit('event', msg);
    });
  });

  app.get('/ping', function (req, res) {
    res.send('pong');
  });

  /**
   * Get all events
   * @param req
   * @param res
   * @private
   */
  function recoverEvents(req, res) {
    const since =
      typeof req.params.timestamp === 'string' ? parseInt(req.params.timestamp, 10) : null;

    function getCreatedSiteEvents(events, next) {
      SiteService.getLastNCreatedSites(null, since, (err, sites) => {
        if (err) {
          console.error('Could not retrieve created sites', err);
        }

        next(
          events.concat(
            (sites || []).map((site) => ({
              type: Events.SITE_CREATED,
              author: site.CREATION_AUTHOR,
              data: JSON.stringify(site),
              date: site.CREATION_DATE,
            }))
          )
        );
      });
    }

    /**
     * Get all "site position updated" events
     * @param  {array}    events
     * @param  {Function} next
     */
    function getMovedSiteEvents(events, next) {
      SiteService.getLastNMovedSites(null, since, (err, sites) => {
        if (err) {
          console.error('Could not retrieve position updated sites', err);
        }

        next(
          events.concat(
            (sites || []).map((site) => ({
              type: Events.SITE_POSITION_UPDATED,
              author: site.POSITION_AUTHOR,
              data: JSON.stringify(site),
              date: site.POSITION_DATE,
            }))
          )
        );
      });
    }

    /**
     * Get all "state changed" events
     * @param  {array}    events
     * @param  {Function} next
     */
    function getStateUpdatedEvents(events, next) {
      SiteService.getLastNStateUpdatedSites(null, since, (err, sites) => {
        if (err) {
          console.error('Could not retrieve state updated sites', err);
        }

        next(
          events.concat(
            (sites || []).map((site) => ({
              type: Events.SITE_ETAT_UPDATED,
              author: site.ETAT_AUTHOR,
              data: JSON.stringify(site),
              date: site.ETAT_DATE,
            }))
          )
        );
      });
    }

    /**
     * Get all "remark added" events
     * @param  {array}    events
     * @param  {Function} next
     */
    function getCreatedRemarqueEvents(events, next) {
      RemarqueService.getLastNCreatedRemarques(null, since, (err, remarques) => {
        if (err) {
          console.error('Could not query list of remarques', err);
        }

        next(
          events.concat(
            (remarques || []).map(function (remarque) {
              let data = {
                ETAT: remarque.site ? remarque.site.ETAT : null,
                DEPARTEMENT: remarque.site ? remarque.site.DEPARTEMENT : null,
                PAYS: remarque.site ? remarque.site.PAYS : null,
              };

              return {
                type: Events.REMARK_ADDED,
                author: remarque.AUTHOR,
                data: JSON.stringify(Object.assign(data, remarque.toJSON({ depopulate: true }))),
                date: remarque.DATE,
              };
            })
          )
        );
      });
    }

    // Get all "photo added" events - TODO

    // Concat and sort
    getCreatedSiteEvents([], function (events) {
      getMovedSiteEvents(events, function (events) {
        getStateUpdatedEvents(events, function (events) {
          getCreatedRemarqueEvents(events, function (events) {
            res.send(events.sort((evt1, evt2) => (evt1.date < evt2.date ? 1 : -1)));
          });
        });
      });
    });
  }
  app.get('/since/:timestamp', recoverEvents);

  httpServer.on('error', (e) => {
    Utils.logError(`Websocket server error: ${e.code} (${JSON.stringify(e)})`);
    process.exit(1);
  });

  // Boot
  const { port } = config.webSockets;
  httpServer.listen(port, function () {
    Utils.log('Websocket server is running and listening on port ' + port);
  });
}

/**
 * Apply filter arguments to a socket
 * @param  {object} socket
 * @param  {object} filter
 * @return {boolean}
 */
function _passFilter(socket, filter) {
  let pass = true;
  Object.keys(filter).forEach(function (propName) {
    pass = pass && socket.handshake.query[propName] === filter[propName];
  });
  return pass;
}

/**
 * Broadcast an event to all connected clients
 * @param {string} eventName
 * @param {*} eventData
 * @param {function} (optional) filter the clients
 */
export function emit(eventName, eventData, filter) {
  if (config.webSockets.enabled !== true) {
    return;
  }

  if (cluster.isWorker) {
    // Ask master to emit the ws event
    process.send({
      action: 'emit-ws-event',
      eventName: eventName,
      eventData: eventData,
      filter: filter,
    });
  } else {
    let emitedCount = 0;
    Object.values(sockets).forEach(function (socket) {
      if (socket && (!filter || _passFilter(socket, filter) === true)) {
        socket.emit(eventName, eventData);
        emitedCount++;
      }
    });

    Utils.log(`[WS] Sent event ${eventName} to ${emitedCount} client(s)`);
  }
}

global.emitWsEvent = emit;

/**
 * Shutdown the websocket server
 */
export function stop() {
  if (shuttingDown) return;
  shuttingDown = true;

  if (httpServer) {
    Utils.logInfo('[WS] Web-socket server shutting down');

    ioServer.close(() => {
      Utils.logInfo('[WS] IO server closed');

      httpServer.close(() => {
        Utils.logInfo('[WS] HTTP server closed');
        // process.exit(0);
      });
    });
  }
}

process.on('exit', stop);
process.on('SIGINT', stop);
