export default {
  sessionDurationInHours: 24,

  diary: {
    resultsPerPage: 50,
  },

  imageResolution: 96,

  tailleDiapos: 1280,
  qualiteDiapos: 93,

  tailleThumbs: 135,
  qualiteThumbs: 90,

  diaposDir: 'diapos',
  thumbsDir: 'thumbs',

  juiceboxEmptyConfig: 'juicebox-empty-config.xml',

  webSockets: {
    enabled: true,
  },
};
