import fs from 'fs';
import papaparse from 'papaparse';
import minimist from 'minimist';
import Site from './models/Site';
import * as DbService from './services/DatabaseService';

const DEFAULT_SITE_ETAT = 5; // état 'non vérifié'
const NUMBER_OF_LINES_TO_SKIP = 1;
const DELIMITER = ';';

/*
 * CLI sample:
 *   ENV=dev node dist/import-csv.js ~/Downloads/export-59.csv 59
 */

if (process.argv.length < 4) {
  console.log(`
  Usage:
    ENV=... node import-csv path/to/csv/file departement(on-2-chars)
`);
  process.exit(1);
}

const roundFloat = (f) => Math.floor(f * 100000) / 100000;

(async function () {
  let exitStatus = 0;
  await DbService.connect();

  const { update } = minimist(process.argv.slice(2));
  if (update) {
    console.info('Update flag ON');
  }

  try {
    const [, , path, departement] = process.argv;

    const data = fs.readFileSync(path, 'UTF-8');
    const lines = data.split(/\r?\n/).splice(NUMBER_OF_LINES_TO_SKIP);
    console.log(`File contains ${lines.length} lines`);

    let linesCount = 0,
      importedCount = 0;
    for (const line of lines) {
      if (!line.length) {
        continue;
      }
      console.log('\n\n' + line);

      const { data } = papaparse.parse(line, { delimiter: DELIMITER });
      console.log(data);

      const [
        IDENTIFIANT, // Identifiant
        commune, // Commune principale
        RAISON_SOCIALE, // Raison sociale
        NOM_USUEL, // Nom usuel
        COMMENTAIRE, // Commentaire
        ADRESSE, // Adresse
        COMMENTAIRE_LOCALISATION, // Commentaire localisation
        ETAT_OCCUPATION, // Etat occupation
        CODE_ACTIVITE, // Code activité
        LIBELLE_ACTIVITE, // Libellé activité
        COMMENTAIRE_ACTIVITE, // Commentaire activité
        X_WGS_84, // X_WGS84
        Y_WGS_84, // Y_WGS84
        _geolocSite, // Géolocalisation site - champ inintéressant
        PRECISION_XY, // "Précision "
        SURFACE_TOTALE, // Surface totale
        SITE_REAMENAGE, // Site réaménagé
        SITE_EN_FRICHE, // Site en friche
        TYPE_DE_REAMENAGEMENT, // Type de réaménagement
        PROJET_DE_REAMENAGEMENT, // Projet de réaménagement
        // Section du cadastre
        // Numéro de la parcelle cadastrale
      ] = data[0];

      /*console.log([
        IDENTIFIANT,
        commune,
        RAISON_SOCIALE,
        NOM_USUEL,
        COMMENTAIRE,
        ADRESSE,
        COMMENTAIRE_LOCALISATION,
        ETAT_OCCUPATION,
        CODE_ACTIVITE,
        LIBELLE_ACTIVITE,
        COMMENTAIRE_ACTIVITE,
        X_WGS_84,
        Y_WGS_84,
        _geolocSite,
        PRECISION_XY,
        SURFACE_TOTALE,
        SITE_REAMENAGE,
        SITE_EN_FRICHE,
        TYPE_DE_REAMENAGEMENT,
        PROJET_DE_REAMENAGEMENT,
      ].join(';'));*/

      if (IDENTIFIANT.length !== 10) {
        console.warn('Ligne vraisemblablement mal formatée donc ignorée: ' + line);
      } else if (!X_WGS_84 || !Y_WGS_84) {
        console.log(`Site ${IDENTIFIANT} ignoré car pas de géolocalisation`);
      } else if (
        NOM_USUEL.startsWith('Dépôt') ||
        NOM_USUEL.startsWith('Décharge') ||
        NOM_USUEL.startsWith('Terril')
      ) {
        console.log(`Site ${IDENTIFIANT} ignoré car pas intéressant (dépôt, décharge)`);
      } else {
        if (update) {
          const existingSite = await Site.findOne({ IDENTIFIANT });
          if (existingSite) {
            console.debug('Skipping existing site ' + IDENTIFIANT);
            continue;
          }
        }

        const site = new Site({
          IDENTIFIANT,
          RAISON_SOCIALE,
          NOM_USUEL,
          COMMENTAIRE,
          ADRESSE,
          COMMENTAIRE_LOCALISATION,
          ETAT_OCCUPATION,
          CODE_ACTIVITE,
          LIBELLE_ACTIVITE,
          COMMENTAIRE_ACTIVITE,
          PRECISION_XY,
          TYPE_DE_REAMENAGEMENT,
          PROJET_DE_REAMENAGEMENT,
        });
        if (SITE_REAMENAGE && SITE_REAMENAGE.toLowerCase() === 'oui') {
          continue;
        }
        if (SITE_EN_FRICHE && SITE_EN_FRICHE.toLowerCase() === 'oui') {
          site.SITE_EN_FRICHE = 1;
          site.IS_ACTIVITE_TERMINEE = 1;
        } else {
          continue;
        }
        site.PAYS = 'FR';
        site.DEPARTEMENT = departement;
        site.ADRESSE = (site.ADRESSE ? `${site.ADRESSE}, ` : '') + commune;
        site.GEO_POINT = {
          type: 'Point',
          coordinates: [roundFloat(parseFloat(X_WGS_84)), roundFloat(parseFloat(Y_WGS_84))],
        };
        if (SURFACE_TOTALE) {
          site.SURFACE_TOTALE = parseFloat(SURFACE_TOTALE);
          if (isNaN(site.SURFACE_TOTALE)) {
            console.error(`Failed to parse SURFACE_TOTALE as float: ${SURFACE_TOTALE}`);
          }
        }
        site.ETAT = DEFAULT_SITE_ETAT;
        site.CREATION_DATE = new Date();

        console.log(site.toJSON());

        await site.save();
        importedCount++;
      }

      linesCount++;
    }
    console.debug(`\n${importedCount} sites imported to database\n`);
  } catch (e) {
    console.error(e);
    exitStatus = 1;
  }

  await DbService.disconnect();
  process.exit(exitStatus);
})();
