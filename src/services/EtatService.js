import Etat from '../models/Etat';

/**
 * Get all etats
 */
export const getAll = () => Etat.find().sort({ order: -1 });
