import Events from '../Events';

import * as RemarqueService from '../services/RemarqueService';
import * as SiteService from '../services/SiteService';

const EVENTS_AMOUNT = 80;

const toCreatedSiteEvent = (site) => ({
  SITE_ID: site._id,
  AUTHOR: site.CREATION_AUTHOR,
  DATE: site.CREATION_DATE.getTime(),
  ETAT: site.ETAT,
  REMARQUE: null,
  NOM_USUEL: site.getLabel(),
  DEPARTEMENT: site.DEPARTEMENT,
  PAYS: site.PAYS,
  ADRESSE: null,
  TYPE: Events.SITE_CREATED,
});

const toPositionUpdatedEvent = (site) => ({
  SITE_ID: site._id,
  AUTHOR: site.POSITION_AUTHOR,
  DATE: site.POSITION_DATE.getTime(),
  ETAT: site.ETAT,
  REMARQUE: null,
  NOM_USUEL: site.getLabel(),
  DEPARTEMENT: site.DEPARTEMENT,
  PAYS: site.PAYS,
  ADRESSE: site.ADRESSE,
  TYPE: Events.SITE_POSITION_UPDATED,
});

const toStateUpdateEvent = (site) => ({
  SITE_ID: site._id,
  AUTHOR: site.ETAT_AUTHOR,
  DATE: site.ETAT_DATE.getTime(),
  ETAT: site.ETAT,
  REMARQUE: null,
  NOM_USUEL: site.getLabel(),
  DEPARTEMENT: site.DEPARTEMENT,
  PAYS: site.PAYS,
  ADRESSE: null,
  TYPE: Events.SITE_ETAT_UPDATED,
});

const toCreatedRemarkEvent = (remarque) => ({
  SITE_ID: remarque.SITE_ID,
  AUTHOR: remarque.AUTHOR,
  DATE: remarque.DATE.getTime(),
  ETAT: remarque.site ? remarque.site.ETAT : null,
  REMARQUE: remarque.REMARQUE,
  NOM_USUEL: null,
  DEPARTEMENT: remarque.site ? remarque.site.DEPARTEMENT : null,
  PAYS: remarque.site ? remarque.site.PAYS : null,
  ADRESSE: null,
  TYPE: Events.REMARK_ADDED,
});

const sortEventsByDate = (evt1, evt2) => (evt1.DATE < evt2.DATE ? 1 : -1);

/**
 * Get diary events since a given time
 * @param {number} since (timestamp in milliseconds)
 */
export async function getAll(since) {

  const [ createdSites, updatePositionSites, stateUpdatedSites, createdRemarks ] = await Promise.all([
     SiteService.getLastNCreatedSites(EVENTS_AMOUNT, since),
     SiteService.getLastNMovedSites(EVENTS_AMOUNT, since),
     SiteService.getLastNStateUpdatedSites(EVENTS_AMOUNT, since),
     RemarqueService.getLastNCreatedRemarques(EVENTS_AMOUNT, since)
  ])

  return createdSites.map(toCreatedSiteEvent)
          .concat(updatePositionSites.map(toPositionUpdatedEvent))
          .concat(stateUpdatedSites.map(toStateUpdateEvent))
          .concat(createdRemarks.map(toCreatedRemarkEvent)).sort(sortEventsByDate);
}
