import mongoose from 'mongoose';

import Remarque from '../models/Remarque';
import * as SiteService from './SiteService';
import { argMissing } from '../helpers/Error';

/**
 * Get remarques by site id
 * @param id
 */
export async function getBySiteId(id) {
  if (!id) {
    throw argMissing('id');
  }
  return await Remarque.find({ SITE_ID: id });
}

/**
 * @param  {Number} amount
 */
export async function getLastNCreatedRemarques(amount, since) {
  let criteria = {};
  if (typeof since === 'number') {
    criteria.DATE = { $gt: new Date(since) };
  }

  return Remarque.find(criteria).sort({ DATE: -1 }).limit(amount).populate('site');
}

/**
 * Add a remark for a site
 * @param {String} remarque
 * @param {Number} siteId
 * @param {String} author
 */
export async function create(remarque, siteId, author) {
  if (!remarque) {
    throw argMissing('remarque')
  }
  if (!siteId) {
    throw argMissing('siteId')
  }
  if (!author) {
    throw argMissing('author')
  }

  // Get site
  const site = await SiteService.getById(siteId)

  if (!site) {
    throw new Error('No site found matching ' + siteId);
  }

  // Perform creation
  const savedRemarque = await new Remarque({
    SITE_ID: siteId,
    AUTHOR: author,
    DATE: new Date(),
    REMARQUE: remarque,
    site: site,
  }).save()

  return savedRemarque;

  // Inject ETAT and DEPARTEMENT values in returned object for Diary
  /*Site.findById(siteId, 'ETAT DEPARTEMENT', function(err, site){
    if (err){
        next(err)
    } else {
        remark._doc.ETAT = site.ETAT
        remark._doc.DEPARTEMENT = site.DEPARTEMENT
        next(null, remark)
    }
  })*/
}
