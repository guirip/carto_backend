import mongoose from 'mongoose';

import * as Utils from '../helpers/Utils';
import config from '../helpers/config';

/**
 * Open the default connection to mongodb
 * @param {object}  additionalOptions
 */
export const connect = (additionalOptions = {}) =>
  mongoose.connect(`mongodb://${config.db.host}:${config.db.port}/${config.db.instance}`, {
    //user : config.db.user,
    //pass : config.db.pass,
    ...additionalOptions,
  });

/**
 * Close the default connection to mongodb
 */
export const disconnect = () => mongoose.disconnect();

// EVENT HANDLERS

/**
 * Handle mongoose connection success event
 * @private
 */
function connectionSuccess() {
  Utils.logInfo('Mongoose connection success');
}
// mongoose.connection.on('connected', connectionSuccess)  // promise instead

/**
 * Handle mongoose connection error event
 * @param err
 * @private
 */
function onError(err) {
  Utils.logError('Mongoose error: ' + err);
}
mongoose.connection.on('error', onError);

/**
 * Handle mongoose connection close event
 * @private
 */
function connectionClosed() {
  Utils.log('Mongoose connection close');
}
mongoose.connection.on('disconnected', connectionClosed);

/**
 * Ping database
 * @param  {Function} next
 */
export const ping = async () => {
  if (!mongoose.connection || !mongoose.connection.db) {
    return {
      isConnectionOk: false,
      message: 'No database connection',
    };
  }

  try {
    const pingResult = await mongoose.connection.db.admin().ping();
    return {
      isConnectionOk: pingResult.ok === 1,
    };
  } catch (e) {
    return {
      isConnectionOk: false,
      message: `Database connection KO: ${e.message || ''}`,
    };
  }
};
