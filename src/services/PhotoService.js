import Photo from '../models/Photo';
import { argMissing } from '../helpers/Error';

/*
  UNMAINTAINED MODULE
*/

/**
 * Get photos by site id
 * @param id
 * @param next
 */
export function getBySiteId(id, next) {
  if (!id) {
    next(argMissing('id'));
  } else {
    Photo.find({ SITE_ID: id }, next);
  }
}

/* Handle upload of a single photo
   * @param $file
   * @param $author
   * @param $siteId
   * @param $commentaire
   *
  function upload($file, $author, $siteId, $commentaire){

      switch ($file['error']){
          case 1:
              // UPLOAD_ERR_INI_SIZE
              throw new FunctionalException('Le fichier '.$file['name'].' dépasse la limite de '.ini_get('upload_max_filesize').' autorisée par le serveur !')
          case 2:
              // UPLOAD_ERR_FORM_SIZE
              throw new FunctionalException('Le fichier '.$file['name'].' dépasse la limite autorisée sur la page web !')
          case 3:
              // UPLOAD_ERR_PARTIAL
              throw new FunctionalException('L\'envoi du fichier '.$file['name'].' a été interrompu !')
          case 4:
              // UPLOAD_ERR_NO_FILE
              throw new FunctionalException('Le fichier '.$file['name'].' est vide !')
          case 6:
              // UPLOAD_ERR_NO_TMP_DIR
              throw new FunctionalException('Erreur serveur: le dossier temporaire pour l\'upload n\'est pas défini !')
          case 7:
              // UPLOAD_ERR_CANT_WRITE
              throw new FunctionalException('Erreur lors de l\'écriture du fichier '.$file['name'].' sur le serveur !')
          case 8:
              // UPLOAD_ERR_EXTENSION
              throw new FunctionalException('L\'envoi du fichier '.$file['name'].' a été interrompu par le serveur !')
          case 0:
              // UPLOAD OK DU FICHIER

              $uniqueName = basename(FileUtil::updateFilenameToAvoidExisting(Config::getDiaposPath().DIRECTORY_SEPARATOR.$file['name']))

              $dirPhotosDest = Config::DIR_PHOTOS.DIRECTORY_SEPARATOR.$uniqueName

              // Déplacement du fichier
              $bMoveSucces = move_uploaded_file($file['tmp_name'], $dirPhotosDest)
              if (!$bMoveSucces) {
                  throw new Exception('Erreur lors du déplacement du fichier '.$uniqueName.' vers le répertoire de stockage.')
              }

              // Création de l'objet image
              ImagickUtil::resize(new GenericImage($dirPhotosDest), Config::TAILLE_DIAPOS, Config::getDiaposPath())

              // Creation des miniatures
              ImagickUtil::resize(new GenericImage($dirPhotosDest), Config::TAILLE_THUMBS, Config::getThumbsPath(), true)

              // Remove tmp file
              unlink($dirPhotosDest)

              // Insert entry to database
              $json = $this->create($uniqueName, $author, $siteId, $commentaire)

              // Add image to juicebox xml gallery
              // @see https://bitbucket.org/guirip/carto/src/719b9d3fa966d98d8e2f62333d752a9b81d464e1/_server-php/entities/Gallery.php?at=master&fileviewer=file-view-default
              $gallery = new Gallery($siteId)

              // @see https://bitbucket.org/guirip/carto/src/719b9d3fa966d98d8e2f62333d752a9b81d464e1/_server-php/entities/Image.php?at=master&fileviewer=file-view-default
              $gallery->add(new Image($uniqueName, $commentaire))

              global $log
              $log->LogInfo('['.basename(__FILE__).'] Upload et redimensionnement avec success de '.$uniqueName)

              return $json
      }
  }*/

/*
   * Insert a photo
   * @param $author
   * @param $siteId
   * @param $commentaire
   * @return string
   * @throws Exception
   *
  function create($filename, $author, $siteId, $commentaire){
      if (!isset($author)){
          throw new Exception("author is missing")
      }
      else if (!isset($siteId)){
          throw new Exception("site id is missing")
      }
      else {
          $int_site_id = intval($siteId)

          $connection = DatabaseService::getConnection()
          try {
              $stmt = $connection->prepare(SELF::SQL_CREATE)
              $stmt->bindParam(':filename',    $filename,    PDO::PARAM_STR)
              $stmt->bindParam(':author',      $author,      PDO::PARAM_STR)
              $stmt->bindParam(':site_id',     $int_site_id, PDO::PARAM_INT)
              $stmt->bindParam(':commentaire', $commentaire, PDO::PARAM_STR)
              $status = $stmt->execute()

              if (!$status){
                  throw new Exception('Photo creation failed')
              }
              return $this->get($connection->lastInsertId())
          } finally {
              $connection = null
          }
      }
  }
  */
