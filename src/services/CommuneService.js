import Commune from '../models/Commune';
import { argMissing } from '../helpers/Error';
import { normalize } from '../helpers/FieldsUtils';

/**
 * Get a commune
 * @param id
 * @return
 * @throws Exception
 */
export async function getById(id) {
  if (!id) {
    throw argMissing('id');
  }
  return await Commune.findById(id);
}

/**
 * Get a commune by code Insee
 * @param codeInsee
 */
export async function getByCodeInsee(codeInsee) {
  if (!codeInsee) {
    throw argMissing('codeInsee');
  }
  return await Commune.findOne({ INSEE: parseInt(codeInsee, 10) });
}

/**
 * Normalize input so it can match city names in database
 * @param {string} name
 * @return {string}
 */
export const normalizeName = (name) =>
  normalize(name)
    .toUpperCase()
    .replace(/^\s?(SAINTE?)/gi, 'ST')
    .replace(/'|-/gi, ' ');
