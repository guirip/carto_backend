import session from 'express-session';
import { createClient } from 'redis';
import RedisStore from 'connect-redis';

import config from '../helpers/config';
import * as Utils from '../helpers/Utils';

export function init() {
  const redisClient = createClient({
    url: `redis://${config.redis.host}:${config.redis.port}`,
    legacyMode: true,
  });

  redisClient.connect();

  redisClient.on('connect', function () {
    Utils.logInfo('Connected to redis');
  });

  redisClient.on('ready', function () {
    Utils.logInfo('Redis ready');
  });

  redisClient.on('error', (err) => {
    Utils.logError('Redis error ' + err);
  });

  return new RedisStore({ client: redisClient });
}
