import { unauthorized } from '../helpers/Error';
import { logInfo } from '../helpers/Utils';

const USER_KEY = 'user_name';

/**
 * Perform basic authentification
 * @param req
 * @return boolean
 */
export function auth(req, res) {
  const { user } = req.params;
  const { secret } = req.body;

  if (secret.startsWith(process.env.PASS)) {
    logInfo(`Auth success for user ${user}`);
    res.cookie(USER_KEY, user);
    return true;
  }
  return false;
}

/**
 * Check user session
 * @param req
 * @param _res
 * @param next
 * @return
 */
export function checkSession(req, _res, next) {
  const user = req.cookies[USER_KEY];
  if (user) {
    next(null, { user });
  } else {
    next(unauthorized());
  }
}

/**
 * Logout user
 * @param _req
 * @param res
 * @return
 */
export function logout(_req, res) {
  res.cookie(USER_KEY, '');
}
