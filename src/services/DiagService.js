import config from '../helpers/config';
import { logError } from '../helpers/Utils';

function getWsPingUrlFromReferer(referer) {
  const refererUrl = new URL(referer);
  const { protocol, hostname } = refererUrl;
  return `${protocol}//${hostname}:${config.webSockets.port}/ping`;
}

export async function pingWS(referer) {
  if (!config.webSockets.port) {
    logError(`Missing configuration 'webSockets.port', cannot ping WS server`);
    return {
      isServerReachable: false,
      message: 'Unable to ping (incomplete server configuration)',
    };
  }

  const pingUrl = getWsPingUrlFromReferer(referer);
  // console.log(` --------- WS ping url is: ${pingUrl}`);

  try {
    const response = await fetch(pingUrl, {
      headers: {
        'Content-Type': 'text/plain',
      },
    });
    const data = await response.text();
    return {
      isServerReachable: data === 'pong',
    };
  } catch (err) {
    logError('Failed to ping websocket server', err);
    return {
      isServerReachable: false,
      message: `WS server seems down: ${err.message || ''}`,
    };
  }
}
