import { CastError } from 'mongoose';
import Site from '../models/Site';
import { argMissing } from '../helpers/Error';
import * as CommuneService from '../services/CommuneService';
import Commune from '../models/Commune';

const updOpts = { new: true };

/**
 * Get a site
 * @param id
 * @return
 * @throws Exception
 */
export async function getById(id) {
  if (!id) {
    throw argMissing('id');
  } else {
    const site = await Site.findById(id)
    if (site) {
      await addCommuneData(site, null);
    }
    return site
  }
}

/**
 * Count sites by departements and etats
 * @param depts
 * @param etats
 * @return Number
 */
export function count(depts, etats) {
  return _getByDeptAndEtat(Site.countDocuments.bind(Site), depts, etats);
}

/**
 * Get sites by departements and etats
 * @param depts
 * @param etats
 * @return Array
 */
export function getByDeptAndEtat(depts, etats) {
  return _getByDeptAndEtat(Site.find.bind(Site), depts, etats);
}

/**
 * @param fn (mongoose schema function to execute)
 * @param {Array} depts
 * @param {Array} etats
 * @param next
 * @private
 * @return
 */
async function _getByDeptAndEtat(fn, depts, etats, next) {
  if (!depts || !depts.length) {
    throw argMissing('depts');
  }
  if (!etats || !etats.length) {
    throw argMissing('etats');
  }
  return await fn({
    DEPARTEMENT: { $in: depts },
    ETAT: { $in: etats.map((etat) => parseInt(etat, 10)) },
    IS_ACTIVITE_TERMINEE: { $ne: 0 },
    SITE_EN_FRICHE: { $ne: 0 },
    SITE_REAMENAGE: { $ne: 1 },
  });
}

/**
 * @param  {float} longitude
 * @param  {float} latitude
 * @param  {int} distanceMax
 * @param  {array of int} etats
 *
 * @doc https://docs.mongodb.com/manual/reference/operator/query/near/
 */
export async function getByEtatAndPosition(longitude, latitude, distanceMax, etats) {
  return await Site.find(
    {
      ETAT: { $in: etats },
      GEO_POINT: {
        $near: {
          $geometry: { type: 'Point', coordinates: [longitude, latitude] },
          $maxDistance: distanceMax * 1000, // meters
        },
      },
    }
  );
}

/**
 * Get all manually created sites
 * @param {number} amount (max results)
 * @param {number} since (timestamp in milliseconds)
 * @return Promise
 */
export function getLastNCreatedSites(amount, since) {
  let criteria = {
    CREATION_AUTHOR: { $ne: null },
  };
  if (typeof since === 'number') {
    criteria.CREATION_DATE = { $gt: new Date(since) };
  }

  return Site.find(
    criteria,
    '_id CREATION_AUTHOR CREATION_DATE ETAT NOM_USUEL RAISON_SOCIALE DEPARTEMENT PAYS'
  )
    .sort({ CREATION_DATE: -1 })
    .limit(amount);
}

/**
 * Get all manually moved sites
 * @param {number} amount (max results)
 * @param {number} since (timestamp in milliseconds)
 * @return Promise
 */
export function getLastNMovedSites(amount, since) {
  let criteria = {
    POSITION_AUTHOR: { $ne: null },
  };
  if (typeof since === 'number') {
    criteria.POSITION_DATE = { $gt: new Date(since) };
  }

  return Site.find(
    criteria,
    '_id POSITION_AUTHOR POSITION_DATE ETAT NOM_USUEL RAISON_SOCIALE DEPARTEMENT PAYS'
  )
    .sort({ POSITION_DATE: -1 })
    .limit(amount);
}

/**
 * Get all manually edited state sites
 * @param {number} amount (max results)
 * @param {number} since (timestamp in milliseconds)
 * @return Promise
 */
export function getLastNStateUpdatedSites(amount, since) {
  let criteria = {
    ETAT_AUTHOR: { $ne: null },
  };
  if (typeof since === 'number') {
    criteria.ETAT_DATE = { $gt: new Date(since) };
  }

  return Site.find(
    criteria,
    '_id ETAT_AUTHOR ETAT_DATE ETAT NOM_USUEL RAISON_SOCIALE DEPARTEMENT PAYS'
  )
    .sort({ ETAT_DATE: -1 })
    .limit(amount);
}

/**
 * Add a site
 * @param values
 * @param author
 */
export async function create(values, author) {
  if (!values.DEPARTEMENT) {
    // Fallback: Google geocoding service sometimes does not provide postal code
    if (values.PAYS === 'FR') {
      values.DEPARTEMENT = await getDepartementFromCommune(values.COMMUNE);
    }
    if (!values.DEPARTEMENT) {
      throw argMissing('DEPARTEMENT');
    }
  }
  if (!values.longitude) {
    throw argMissing('longitude');
  }
  if (!values.latitude) {
    throw argMissing('latitude');
  }
  if (!values.ETAT) {
    throw argMissing('ETAT');
  }
  if (!author) {
    throw argMissing('author');
  }

  const site = new Site(values);

  site.CREATION_AUTHOR = author;
  site.CREATION_DATE = new Date();
  site.GEO_POINT = {
    type: 'Point',
    coordinates: [values.longitude, values.latitude],
  };

  return await site.save();
}

/**
 * Common logic to retrieve departement from city name
 * @param  {string} commune
 * @return {string}
 */
async function getDepartementFromCommune(commune) {
  if (!commune) {
    return;
  }
  const normalized = CommuneService.normalizeName(commune);
  const communeDoc = await Commune.findOne({ COMMUNE: normalized });
  if (communeDoc?.CP) {
    const deptAsString = String(communeDoc.CP);
    return deptAsString.substring(0, deptAsString.length === 4 ? 1 : 2);
  }
}

/**
 * Update a site position
 * @param {string} id
 * @param {object} values
 * @param {string} author
 */
export async function updatePosition(id, values, author) {
  if (!values.departement) {
    // Fallback: Google geocoding service sometimes does not provide postal code
    if (values.pays === 'FR') {
      values.departement = await getDepartementFromCommune(values.commune);
    }
    if (!values.departement) {
      throw argMissing('departement');
    }
  }
  if (!id) {
    throw argMissing('id');
  }
  if (!values.x) {
    throw argMissing('x');
  }
  if (!values.y) {
    throw argMissing('y');
  }
  if (!author) {
    throw argMissing('author');
  }

  return await Site.findByIdAndUpdate(
    id,
    {
      'GEO_POINT.coordinates': [values.x, values.y],
      ADRESSE: values.adresse,
      DEPARTEMENT: values.departement,
      PAYS: values.pays,
      POSITION_AUTHOR: author,
      POSITION_DATE: new Date(),
      CODE_INSEE: null,
      X_LAMBERT_2E: null,
      Y_LAMBERT_2E: null,
      X_LAMBERT_93: null,
      Y_LAMBERT_93: null,
      X_ADRESSE_LAMBERT_2E: null,
      Y_ADRESSE_LAMBERT_2E: null,
      X_WGS_84: null,
      Y_WGS_84: null,
      communeId: null,
    },
    updOpts
  );
}

/**
 * Update a site etat
 * @param id
 * @param etat
 * @param author
 */
export async function updateEtat(id, etat, author) {
  if (!id) {
    throw argMissing('id');
  }
  if (!etat) {
    throw argMissing('etat');
  }
  if (!author) {
    throw argMissing('author');
  }

  const site = await Site.findByIdAndUpdate(
    id,
    {
      ETAT: etat,
      ETAT_AUTHOR: author,
      ETAT_DATE: new Date(),
    },
    updOpts
  )
  if (site) {
    await addCommuneData(site, null);
  }
  return site
}

/**
 * @param site
 * @param commune
 * @private
 */
async function addCommuneData(site, commune) {
  if (commune) {
    setCpAndCommune(site, commune);
  } else {
    if (site.communeId) {
      // Legacy
      const communeFetched = await CommuneService.getById(site.communeId);
      setCpAndCommune(site, communeFetched)

    } else if (site.CODE_INSEE) {
      // Legacy
      const communeFetched = await CommuneService.getByCodeInsee(site.CODE_INSEE);
      setCpAndCommune(site, communeFetched)
    }
  }
}

/**
 * @param site
 * @param commune
 * @private
 */
function setCpAndCommune(site, commune) {
  site._doc.CP = commune.CP;
  site._doc.COMMUNE = commune.COMMUNE;
}
