const DOMAIN = 'carto-integration.heytonton.fr';

export default {
  appRoot: '',
  port: 3014,
  corsAllowedOrigins: [`https://${DOMAIN}`],

  useHttps: true,
  keyPath: `/opt/node/certs/${DOMAIN}/privkey.pem`,
  certPath: `/opt/node/certs/${DOMAIN}/fullchain.pem`,

  redis: {
    host: 'localhost',
    port: 6379,
  },

  db: {
    host: '127.0.0.1',
    port: 27017,
    instance: 'carto-integration',
    user: 'carto',
    passwd: 'passmo2!carto',
  },

  webSockets: {
    port: 3004,
  },
};
