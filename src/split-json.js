(async () => {
  'use strict';

  const inquirer = require('inquirer'),
    fs = require('fs-extra'),
    path = require('path');

  console.log('');

  // Check arguments count
  if (process.argv.length < 5) {
    console.error(`Missing arguments.
Usage: node split-json path/to/file.json maxEntries output/path

      e.g: node src/split-json.js ../CARTO_DATA/json/site.json 30000 ../CARTO_DATA/json-splitted
  `);
    process.exit(1);
  }

  // `maxEntries`
  const maxEntries = parseInt(process.argv[3], 10);

  // data
  const dataPath = process.cwd() + '/' + process.argv[2],
    data = require(dataPath);
  if (Array.isArray(data) !== true) {
    console.error('data is not an array');
    process.exit(1);
  }
  const fileName = path.basename(dataPath, '.json');

  // output path
  const outputPath = process.argv[4];

  if (fs.existsSync(outputPath) !== true) {
    // Output dir does not exist, create it
    fs.ensureDirSync(outputPath);
  } else {
    const outputStats = fs.statSync(outputPath);
    if (outputStats.isDirectory() !== true) {
      // Ouput exists and is not a directory
      console.error('Ouput path exists and is not a directory. Aborting.');
      process.exit(1);
    }

    // Output dir exists

    const children = fs.readdirSync(outputPath);
    if (children.length > 0) {
      // Directory has content
      const CHOICE_DELETE = 'Delete its content',
        CHOICE_ABORT = 'Abort';

      const { CHOICE_MADE } = await inquirer.prompt([
        {
          type: 'list',
          name: 'CHOICE_MADE',
          message: 'Output directory has existing content, what should we do?',
          choices: [CHOICE_DELETE, CHOICE_ABORT],
        },
      ]);

      switch (CHOICE_MADE) {
        case CHOICE_DELETE:
          children.forEach((child) => {
            const childPath = path.join(outputPath, child);
            console.log(`Deleting ${childPath}`);
            fs.removeSync(childPath);
          });
          break;

        case CHOICE_ABORT:
          process.exit(1);

        default:
          console.warn('Unexpected choice');
          process.exit(1);
      }
    }
  }

  console.info('\nProceeding');

  // PROCEED

  let index = 0,
    startIndex,
    stopIndex,
    batch;

  do {
    (startIndex = index * maxEntries), (stopIndex = startIndex + maxEntries);

    batch = data.slice(startIndex, stopIndex);
    if (batch.length === 0) {
      console.log('Done.');
    } else {
      const outputBatchFilePath = path.join(outputPath, `${fileName}.${index}.json`);
      console.log(
        `From ${startIndex} to ${stopIndex}: ${batch.length} entries found. Writting to ${outputBatchFilePath}`
      );

      fs.writeJsonSync(outputBatchFilePath, batch);
    }
    index++;
  } while (batch.length);

  process.exit(0);
})();
