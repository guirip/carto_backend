/**
 * Return an error with:
 *  - a generic message indicating that an argument is missing
 *  - a 'not found' http code
 * @return {Error}
 */
export function unauthorized() {
  const err = new Error('unauthorized');
  err.httpCode = 401;
  return err;
}

/**
 * Return an error with:
 *  - a generic message indicating that an argument is missing
 *  - a 'bad request' http code
 * @param  {String} argName : argument name
 * @return {Error}
 */
export function argMissing(argName) {
  const err = new Error(argName + ' argument is missing');
  err.httpCode = 400;
  return err;
}

/**
 * Return an error with:
 *  - a generic message indicating that an argument is missing
 *  - a 'bad request' http code
 * @param  {String} msg : reason
 * @return {Error}
 */
export function badRequest(msg) {
  const err = new Error(msg);
  err.httpCode = 400;
  return err;
}
