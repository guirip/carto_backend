import deepmerge from 'deepmerge';

import commonConfig from '../config.common';
import testConfig from '../config.test';
import devConfig from '../config.dev';
import inteConfig from '../config.inte';
import prodConfig from '../config.prod';

import { logInfo } from './Utils';

const configs = {
  common: commonConfig,
  test: testConfig,
  dev: devConfig,
  inte: inteConfig,
  prod: prodConfig,
};

const envType = process.env.ENV ?? process.env.NODE_ENV;
const envConfig = configs[envType];

let mergedConfig;
if (!envConfig) {
  console.error(`Missing configuration file for environment: ${envType}`);
  process.exit(1);
} else {
  logInfo(`Loading configuration for environnement: ${envType}`);
  mergedConfig = deepmerge(configs.common, envConfig);
}

export default mergedConfig;
