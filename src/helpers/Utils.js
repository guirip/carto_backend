import cluster from 'cluster';

export const sendResponse = (res) =>
  function (err, data) {
    if (err) {
      res.status(err.httpCode || 500).send();
    } else {
      res.status(200);
      if (data) {
        res.json(data);
      } else {
        res.send('');
      }
    }
  };

export async function genericSurround(res, promise) {
  const next = sendResponse(res)
  try {
    const data = await promise;
    next(null, data)
  } catch (e) {
    console.error(e)
    next(e)
  }
}

/**
 * Log something prepending date & time
 * @private
 */
function _log(string, others) {
  console.log(
    `[${getCurrentDateTimeLabel()}] ${
      cluster.isWorker ? '(Worker ' + cluster.worker.process.pid + ') ' : ''
    } ${string}`,
    ...(others ?? [])
      .filter((o) => o)
      .map((o) => {
        if (typeof o === 'object') {
          return JSON.stringify(o);
        }
        return o;
      })
  );
}

export function log(string, ...others) {
  _log(string, others);
}

export async function logInfo(string, ...others) {
  log(string, others.length ? others : undefined);
}

export async function logWarn(string, ...others) {
  log(string, others.length ? others : undefined);
}

export async function logError(string, ...others) {
  log(string, others.length ? others : undefined);
}

/**
 * Return a current date & time label
 * @private
 */
const getCurrentDateTimeLabel = () => new Date().toISOString();
