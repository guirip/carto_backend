#!/usr/bin/env node

/**
 * NB: For large files (sites for instance), it's needed to allow more memory :
 * $ node --max-old-space-size=4000 import-json.js path/to/sites.json Site
 */
(async function () {
  'use strict';

  const Utils = require('./helpers/Utils'),
    path = require('path');

  function usage() {
    Utils.log('Usage: NODE_ENV=development node import-json path/to/file modelName [skipClear]');
  }

  if (!process.env.NODE_ENV) {
    Utils.logError('Missing NODE_ENV env var');
    usage();
    process.exit(1);
  }

  const dbService = require('./services/DatabaseService');

  if (process.argv.length < 4) {
    Utils.logError('Missing arguments');
    usage();
    process.exit(1);
  }

  // do something when app is closing
  process.on('exit', exitHandler);

  // catches ctrl+c event
  process.on('SIGINT', exitHandler);

  // catches uncaught exceptions
  process.on('uncaughtException', exitHandler);

  function exitHandler(code) {
    Utils.log('Process terminated, code: ' + code);
  }

  async function exitOk() {
    // Close mongoose connection, then exit with code 0
    await dbService.disconnect();
    process.exit(0);
  }

  // Require json data
  let dataPath = process.argv[2],
    data;
  try {
    data = require(path.join(process.cwd(), dataPath));
    Utils.logInfo('About to import content of: ' + dataPath);
  } catch (e) {
    Utils.logError('File does not exist: ' + dataPath);
    process.exit(1);
  }

  // Open mongoose connection
  try {
    await dbService.connect(
      {
        autoReconnect: true,
        useUnifiedTopology: false, // prevents MongoTimeoutError during large db update
      },
      true
    );
    Utils.logInfo('Database connection OK');
  } catch (e) {
    Utils.logError('Failed to connect to database', e);
    process.exit(1);
  }

  // Ability to skip existing data clear (useful when importing a collection splitted in batches)
  let skipClear = false;
  if (process.argv.length === 5) {
    if (process.argv[4] === 'skipClear') {
      skipClear = true;
    } else {
      Utils.log('Unexpected fourth argument. Only possible value is: skipClear');
      usage();
      process.exit(1);
    }
  }

  const modelName = process.argv[3],
    Site = require('./models/Site'),
    model = modelName === 'Site' ? Site : require('./models/' + modelName);

  /**
   * @param entry
   * @param next
   * @return
   */
  const insert = async (entry) => model.create(entry);

  /**
   * @param item
   * @param next
   * @return
   */
  const getNewSiteId = async (item) => {
    const site = Site.findOne({ OLD_ID: item.SITE_ID });
    if (!site) {
      throw new Error('no match for Site.SITE_ID=' + item.SITE_ID);
    }
    return site[0]._id;
  };

  // Clear existing data
  if (skipClear !== true) {
    const result = await model.deleteMany();
    Utils.logInfo('Cleared ' + result.deletedCount + ' existing items');
  }

  // Start import
  Utils.logInfo('importing ' + data.length + ' ' + modelName + 's');

  // PHOTO & REMARQUE

  if (modelName === 'Photo' || modelName === 'Remarque') {
    // Need to replace the old mysql site id with the new mongodb id

    const insertItem = async (item) => {
      let itemName;
      if (modelName === 'Photo') {
        itemName = item.FILENAME;
      } else if (modelName === 'Remarque') {
        itemName = 'remarque de ' + item.AUTHOR + ' le ' + item.DATE;
      }
      Utils.log('will insert ' + itemName + ' (SITE_ID:' + item.SITE_ID + ')');

      const [err, siteId] = await getNewSiteId(item);
      if (err) {
        Utils.logError(err);
        process.exit(1);
      }

      item.SITE_ID = siteId;
      item.site = siteId;
      await insert(item);

      Utils.log('has inserted ' + itemName + ' (updated SITE_ID:' + item.SITE_ID + ')' + '\n');
    };

    for (let i = 0; i < data.length; i++) {
      await insertItem(data[i]);
      Utils.log(Math.round(((i * 100) / data.length) * 100) / 100 + '%\n');
    }
  }

  // ETAT
  else if (modelName === 'Etat') {
    const insertEtat = async (item) => {
      if (typeof item.order !== 'number') {
        item.order = parseInt(item.id, 10);
      }
      Utils.log('Inserting etat: ' + JSON.stringify(item));
      await insert(item);
    };

    for (let i = 0; i < data.length; i++) {
      await insertEtat(data[i]);
    }
    await insertEtat({ id: 11, libelle: 'Royaume démoli', order: 0.5 });
    await insertEtat({ id: 12, libelle: 'A surveiller', order: 5.5 });
  } else {
    // Insert the whole json data
    await insert(data);
  }

  exitOk();
})();
