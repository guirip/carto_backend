import express from 'express';
import * as Utils from '../helpers/Utils';

const router = express.Router();
export const root = '/ping';
export const getRouter = () => router;

// GET handlers

/**
 * @param _req
 * @param res
 * @private
 */
async function ping(_req, res) {
  Utils.sendResponse(res)(null, 'pong');
}
router.get('/', ping);
