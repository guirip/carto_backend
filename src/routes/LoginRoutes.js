import express from 'express';

import { argMissing } from '../helpers/Error';
import { sendResponse } from '../helpers/Utils';
import * as LoginService from '../services/LoginService';

const router = express.Router();
export const root = '/login';
export const getRouter = () => router;

// GET handlers

/**
 * @api {get} / Get info from current session
 * @apiDescription Get current session data
 * @apiName check
 * @apiGroup Login
 * @apiPermission authenticated
 *
 * @apiSuccess {String} user Logged-in user name
 *
 * @param req
 * @param res
 * @private
 */
function checkSession(req, res) {
  LoginService.checkSession(req, res, sendResponse(res));
}
router.get('/', checkSession);

// POST handlers

/**
 * @api {post} /:user Perform authentification
 * @apiName auth
 * @apiGroup Login
 *
 * @apiParam (payload) {String} secret password
 *
 * @param req
 * @param res
 * @private
 */
function auth(req, res) {
  if (!req.params.user) {
    sendResponse(res)(argMissing('user'));
  } else {
    if (LoginService.auth(req, res)) {
      res.sendStatus(200);
    } else {
      res.sendStatus(401);
    }
  }
}
router.post('/:user', auth);

// DELETE handlers

/**
 * @api {delete} / End session
 * @apiName logout
 * @apiGroup Login
 *
 * @param req
 * @param res
 * @private
 */
function logout(req, res) {
  LoginService.logout(req, res);
  sendResponse(res)();
}
router.post('/', logout);
