const router = express.Router();

import express from 'express';

import asyncMiddleware from '../helpers/asyncMiddleware';
import * as Utils from '../helpers/Utils';
import { ping as pingDB } from '../services/DatabaseService';
import { pingWS } from '../services/DiagService';
import { checkSession } from '../services/LoginService';

export const root = '/diag';
export const getRouter = () => router;

const STATUS = {
  ok: 'OK',
  ko: 'KO',
};

// Check session for all the routes
router.use(checkSession);

// GET handlers

/**
 * Get server diagnostic
 * @param req
 * @param res
 * @private
 */
async function getDiag(req, res) {
  const dbStatus = await pingDB();
  const wsStatus = await pingWS(req.headers.referer);

  Utils.sendResponse(res)(null, {
    DB: {
      status: dbStatus.isConnectionOk ? STATUS.ok : STATUS.ko,
      message: dbStatus.message,
    },
    WS: {
      status: wsStatus.isServerReachable ? STATUS.ok : STATUS.ko,
      message: wsStatus.message,
    },
  });
}
router.get('/', asyncMiddleware(getDiag));
