import express from 'express';

import asyncMiddleware from '../helpers/asyncMiddleware';
import * as Utils from '../helpers/Utils';
import { getAll } from '../services/EtatService';
import { checkSession } from '../services/LoginService';

const router = express.Router();
export const root = '/etat';
export const getRouter = () => router;

// Check session for all the routes
router.use(checkSession);

// GET handlers

/**
 * @api {get} / Get all etats
 * @apiName getAll
 * @apiGroup Etat
 * @apiPermission authenticated
 *
 * @apiSuccess {Array} etats All sites
 *
 * @param _req
 * @param res
 * @private
 */
async function getEtats(_req, res) {
  Utils.sendResponse(res)(null, await getAll());
}
router.get('/', asyncMiddleware(getEtats));
