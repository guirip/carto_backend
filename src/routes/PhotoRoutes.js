import express from 'express';

import * as Utils from '../helpers/Utils';
import * as PhotoService from '../services/PhotoService';
import { checkSession } from '../services/LoginService';

const router = express.Router();
export const root = '/photo';
export const getRouter = () => router;

// Check session for all the routes
router.use(checkSession);

// GET handlers

/**
 * Get photos for a site
 * @param req
 * @param res
 * @private
 */
function getPhotos(req, res) {
  PhotoService.getBySiteId(req.params.siteId, Utils.sendResponse(res));
}
router.get('/site/:siteId', getPhotos);
