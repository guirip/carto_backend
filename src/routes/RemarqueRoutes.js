import express from 'express';

import { genericSurround, sendResponse } from '../helpers/Utils';
import { checkSession } from '../services/LoginService';
import * as RemarqueService from '../services/RemarqueService';
import asyncMiddleware from '../helpers/asyncMiddleware';

const router = express.Router();
export const root = '/remarque';
export const getRouter = () => router;

// Check session for all the routes
router.use(checkSession);

// GET handlers

/**
 * @api {get} /site/:id Get remarks by site id
 * @apiName getBySiteId
 * @apiGroup Remarque
 * @apiPermission authenticated
 *
 * @apiParam {Number} id site id
 *
 * @apiSuccess {Array} remarques All remarks for this site
 *
 * @apiError id Value is missing
 *
 * @param req
 * @param res
 * @private
 */
async function getBySiteId(req, res) {
  await genericSurround(res, RemarqueService.getBySiteId(req.params.id));
}
router.get('/site/:id', asyncMiddleware(getBySiteId));

/**
 *
 * @api {post} /create/author/:author Create a remark
 * @apiName create
 * @apiGroup Remarque
 * @apiPermission authenticated
 *
 * @apiParam {Number} siteId site id
 * @apiParam {String} author name of the person responsible for this creation
 * @apiParam (payload) {String} remarque content
 *
 * @apiSuccess {Object} site Created remark
 *
 * @apiError siteId Value is missing
 * @apiError author Value is missing
 * @apiError remarque Value is missing
 *
 * @param req
 * @param res
 * @private
 */
async function create(req, res) {
  const next = sendResponse(res)
  try {
    const created = await RemarqueService.create(
      req.body.remarque,
      req.params.siteId,
      req.params.author,
    );
    next(null, {
      ...created.toJSON({ depopulate: true }),
      ETAT: created.site ? created.site.ETAT : null,
      DEPARTEMENT: created.site ? created.site.DEPARTEMENT : null,
      PAYS: created.site ? created.site.PAYS : null,
    })
  } catch (e) {
    next(e)
  }
}
router.post('/create/site/:siteId/author/:author', asyncMiddleware(create));
