import express from 'express';
import asyncMiddleware from '../helpers/asyncMiddleware';
import { badRequest } from '../helpers/Error';

import { genericSurround, sendResponse } from '../helpers/Utils';
import { checkSession } from '../services/LoginService';
import * as SiteService from '../services/SiteService';

const router = express.Router();
export const root = '/site';
export const getRouter = () => router;

// Check session for all the routes
router.use(checkSession);

// GET handlers

/**
 * @api {get} /count/depts/:departements/etats/:etats Count sites
 * @apiDescription Count sites matching these departements and etats
 * @apiName count
 * @apiGroup Site
 * @apiPermission authenticated
 *
 * @apiParam {comma-separated-list} departements Filter sites to these 'departements'
 * @apiParam {comma-separated-list} etats Filter sites to these 'etats'
 *
 * @apiSuccess {Number} total corresponding sites count
 *
 * @apiError departements Value is missing
 * @apiError etats Value is missing
 *
 * @param req
 * @param res
 * @private
 */
async function count(req, res) {
  const count = await SiteService.count(
    req.params.departements.split(','),
    req.params.etats.split(',')
  );
  res.json({ count });
}
router.get('/count/depts/:departements/etats/:etats', asyncMiddleware(count));

/**
 * @api {get} /depts/:departements/etats/:etats Filter sites
 * @apiDescription Get sites matching these departements and etats
 * @apiName getByDeptAndEtat
 * @apiGroup Site
 * @apiPermission authenticated
 *
 * @apiParam {comma-separated-list} departements Filter sites to these 'departements'
 * @apiParam {comma-separated-list} etats Filter sites to these 'etats'
 *
 * @apiSuccess {Array} sites Matching sites
 *
 * @apiError departements Value is missing
 * @apiError etats Value is missing
 *
 * @param req
 * @param res
 * @private
 */
async function getByDeptAndEtat(req, res) {
  const sites = await SiteService.getByDeptAndEtat(
    req.params.departements.split(','),
    req.params.etats.split(',')
  );
  res.json(sites);
}
router.get('/depts/:departements/etats/:etats', asyncMiddleware(getByDeptAndEtat));

/**
 * @api {get} /:id Get a site by its id
 * @apiName getById
 * @apiGroup Site
 * @apiPermission authenticated
 *
 * @apiParam {Number} id site id
 *
 * @apiSuccess {Object} site Matching site
 *
 * @apiError id Value is missing
 *
 * @param req
 * @param res
 * @private
 */
async function getById(req, res) {
  await genericSurround(res, SiteService.getById(req.params.id));
}
router.get('/:id', asyncMiddleware(getById));

/**
 * @param req
 * @param res
 * @private
 */
async function getSitesNear(req, res) {
  const next = sendResponse(res)

  let { longitude, latitude, distanceMax, etats } = req.params;

  // Parse and check coordinates
  longitude = parseFloat(longitude);
  latitude = parseFloat(latitude);
  if (isNaN(longitude) || isNaN(latitude)) {
    next(
      badRequest(
        `Coordonnées invalides: longitude: ${req.params.longitude} et latitude: ${req.params.latitude}`
      )
    );
    return;
  }

  // Parse and check distance
  distanceMax = parseInt(distanceMax, 10);
  if (isNaN(distanceMax)) {
    next(badRequest(`Distance invalide: ${req.params.distanceMax}`));
    return;
  }

  // Parse and check etats
  etats = etats.split(',').map((etat) => parseInt(etat, 10));
  if (etats.length === 0) {
    next(badRequest('Un état au moins doit être sélectionné'));
    return;
  }

  try {
    const sites = await SiteService.getByEtatAndPosition(
      longitude,
      latitude,
      distanceMax,
      etats,
    )
    next(null, sites)
  } catch (e) {
    netx(e)
  }
}
router.get('/near/lon/:longitude/lat/:latitude/dist/:distanceMax/etats/:etats', asyncMiddleware(getSitesNear));

// POST handlers

/**
 * @api {post} /create/author/:author Create a site
 * @apiName create
 * @apiGroup Site
 * @apiPermission authenticated
 *
 * @apiParam {String} author name of the person responsible for this creation
 * @apiParam (payload) {Number} DEPARTEMENT departement
 * @apiParam (payload) {Float} longitude
 * @apiParam (payload) {Float} latitude
 * @apiParam (payload) {Number} ETAT
 *
 * @apiSuccess {Object} site Created site
 *
 * @apiError DEPARTEMENT Value is missing
 * @apiError longitude Value is missing
 * @apiError latitude Value is missing
 * @apiError ETAT Value is missing
 * @apiError author Value is missing
 *
 * @param req
 * @param res
 * @private
 */
async function create(req, res) {
  const siteCreated = await SiteService.create(req.body, req.params.author);
  sendResponse(res)(null, siteCreated.toJSON());
}
router.post('/create/author/:author', asyncMiddleware(create));

/**
 * @api {post} /:id/position/author/:author Update a site position
 * @apiName updatePosition
 * @apiGroup Site
 * @apiPermission authenticated
 *
 * @apiParam {Number} id site id
 * @apiParam {String} author name of the person responsible for this update
 * @apiParam (payload) {Float} x x coordinate
 * @apiParam (payload) {Float} y y coordinate
 *
 * @apiSuccess {Object} site Updated site
 *
 * @apiError id Value is missing
 * @apiError x Value is missing
 * @apiError y Value is missing
 * @apiError departement Value is missing
 * @apiError author Value is missing
 *
 * @param req
 * @param res
 * @private
 */
async function updatePosition(req, res) {
  const siteUpdated = await SiteService.updatePosition(req.params.id, req.body, req.params.author);
  sendResponse(res)(null, siteUpdated.toJSON());
}
router.post('/:id/position/author/:author', asyncMiddleware(updatePosition));

/**
 * @api {post} /:id/etat/author/:author Update a site etat
 * @apiName updateEtat
 * @apiGroup Site
 * @apiPermission authenticated
 *
 * @apiParam {Number} id site id
 * @apiParam {String} author name of the person responsible for this update
 * @apiParam (payload) {Number} etat Site etat
 *
 * @apiSuccess {Object} site Updated site
 *
 * @apiError id Value is missing
 * @apiError etat Value is missing
 * @apiError author Value is missing
 *
 * @param req
 * @param res
 * @private
 */
async function updateEtat(req, res) {
  await genericSurround(res, SiteService.updateEtat(req.params.id, req.body.etat, req.params.author));
}
router.post('/:id/etat/author/:author', asyncMiddleware(updateEtat));
