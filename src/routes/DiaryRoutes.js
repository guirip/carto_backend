import express from 'express';

import asyncMiddleware from '../helpers/asyncMiddleware';
import { checkSession } from '../services/LoginService';
import { getAll } from '../services/DiaryService';
import { sendResponse } from '../helpers/Utils';

const router = express.Router();
export const root = '/diary';
export const getRouter = () => router;

// Check session for all the routes
router.use(checkSession);

// GET handlers

/**
 * Get all events
 * @param req
 * @param res
 * @private
 */
async function getEvents(req, res) {
  sendResponse(res)(null, await getAll(req.query.since));
}
router.get('', asyncMiddleware(getEvents));
