#!/usr/bin/env node

/**
 * NB: For large files (sites for instance), it's needed to allow more memory :
 * $ node --max-old-space-size=4000 import-json.js path/to/sites.json Site
 */
(async function () {
  'use strict';

  if (!process.env.NODE_ENV) {
    console.error('Missing env property. e.g:  NODE_ENV=development node import-json.js ...');
    process.exit(1);
  }

  const dbService = require('./services/DatabaseService'),
    Utils = require('./helpers/Utils');

  // Open mongoose connection
  dbService.connect({
    autoReconnect: true,
    useUnifiedTopology: false, // prevents MongoTimeoutError during large db update
  });

  async function exitOk() {
    // Close mongoose connection, then exit with code 0
    await dbService.disconnect();
    process.exit(0);
  }

  const SiteService = require('./services/SiteService');
  const RemarqueService = require('./services/RemarqueService');

  /*
    SiteService.getLastNStateUpdatedSites(30, function(err, data) {

      console.log('\n\nerr: '+JSON.stringify(err))
      console.log('\n\ndata: '+JSON.stringify(data))
      console.log('\n\n')

      exitOk()
    })
    */

  /*SiteService.getById('5f22def91a6bf450250b92e1', function(err, site) {

      console.log(err)

      const label = site.getLabel()

      console.log(site.getLabel())

      exitOk()
    })*/

  RemarqueService.getLastNCreatedRemarques(10, 1596288196838, function (err, data) {
    console.log(err);

    console.log(JSON.stringify(data.map((rem) => rem.DATE)));

    exitOk();
  });
})();
