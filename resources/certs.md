
## Handling cert renewal

Node instance needs to read the certificate and the private key at boot time.
Certbot keeps symlinks pointing to the current version at this path:
/etc/letsencrypt/live/carto.heytonton.fr/

 - privkey.pem - Private key for the certificate.
 - fullchain.pem - All certificates, including server certificate (aka leaf certificate or end-entity certificate). The server certificate is the first one in this file, followed by any intermediates.

The problem is that these files don't have a read access for user www

One solution is to use a post renewal hook. doc: https://certbot.eff.org/docs/using.html#renewing-certificates
An executable file must be placed at path: /etc/letsencrypt/renewal-hooks/deploy
Its content should be:


#!/bin/bash
echo "Letsencrypt renewal hook running..."
echo "RENEWED_DOMAINS=$RENEWED_DOMAINS"
echo "RENEWED_LINEAGE=$RENEWED_LINEAGE"

if grep --quiet "carto.heytonton.fr" <<< "$RENEWED_DOMAINS"; then
  CERT_DEST_PATH=/opt/node/certs/carto.heytonton.fr/

  echo "Copying renewed cert for carto backend"
  rm -f /opt/node/certs/carto.heytonton.fr/* && \
  cp $RENEWED_LINEAGE/privkey.pem $CERT_DEST_PATH && \
  cp $RENEWED_LINEAGE/fullchain.pem $CERT_DEST_PATH && \
  chown www:www $CERT_DEST_PATH/*.pem && \

  # restart pm2
  pkill -9 "node /opt/node/" && \
  sleep 4 && \
  systemctl restart pm2-www.service
fi


To test, execute: certbot certonly --force-renewal -d carto.heytonton.fr

I had to stop because of message: Error creating new order :: too many certificates already issued for exact set of domains: carto.heytonton.fr: see https://letsencrypt.org/docs/rate-limits/