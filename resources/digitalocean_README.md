
# README : RECAP INSTALL SUR DIGITAL OCEAN

https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-20-04
https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-20-04

## Notes

- OVH: rg47837-ovh / classique + màj.

## Checklist

- create user: www / passmo2!www
- add user www to sudoers
- enabled OpenSSH through firewall ufw
- change current user to www
- install apache2
- allow apache 80 and 443 through the firewall
- create virtual hosts
- enable apache rewrite mod
- deploy some web apps using user www
- redirect vadrouilles.fr to the new server
- deploy memo-git
- install certbot (using commands from website)
- install nvm (using command from website)
- install redis-server (using linux packages)
- install mongodb-server (using linux packages)
- allow used port in the firewall configuration
  - ufw allow PORT
  - NB: due to current apache configuration (ProxyPass), only the websocket server needs to be directly accessible
- import json data to mongo carto instance
- install pm2 and enable startup with system
  - `pm2 startup` (execute printed command)
- add carto node backend to pm2 process list
  - integration
    - `cd /opt/node/carto-integration-backend && npm i && npm run build`
    - ` ENV=inte PASS=... NODE_ENV=production pm2 start /opt/node/carto-integration-backend/dist/start.js --name "carto backend integration"`

  - production
    - `cd /opt/node/carto_backend && npm i && npm run build`
    - ` PASS=... pm2 start npm --name="carto backend production" -- start`
  - `pm2 save`
  - NB: once saved, pm2 outputs a dump in `/home/www/.pm2/dump.pm2`, which can be reused with `pm2 resurrect` command
- carto client build + deploy
- enable https for virtual hosts

---

### Add ssh key via DigitalOcean console

---

### Set french locale (?)

    apt-get install language-pack-fr
    vim /etc/default/locale

then set content to :

    LANG="fr_FR.UTF-8"
    LC_MESSAGES=POSIX

---

### Check/update system date

Show current date/time :

    date

if needed configure the timezone with command:

    sudo dpkg-reconfigure tzdata

---

### Install redis-server

It is used to manage http sessions.

    sudo apt-get install redis-server

---

### Install node.js using NVM

<https://github.com/nvm-sh/nvm#installing-and-updating>

Then install the latest Node LTS

`nvm ls-remote --lts`

e.g: `nvm install v18.14.0`

NB: when upgrading, use the proper flag to reinstall global packages! or bins such as `pm2` won't be available after an upgrade.

e.g: `nvm install v20.11.2 --reinstall-packages-from=v18.14.0`

---

### Install mongodb

    sudo apt-get install mongodb-server

---

### Import json files

Import each json file using script `src/import-json.js`:

    env=dev|prod node import-json path/to/file.json SchemaName

NB: For large files it's needed to allow more memory:

    env=dev|prod node --max-old-space-size=4000 src/import-json ~/carto-data/sites.json Site

NB2: If option `max-old-space-size` is not enough, json files can be splitted using:

    node split-json path/to/file.json maxEntries output/path

e.g

    node src/split-json.js ../CARTO_DATA/json/site 5000 ../CARTO_DATA/json-splitted

Then compress the files:

    tar -czf site0.tar.gz site.0.json
    tar -czf site1.tar.gz site.1.json
    ...

Send them (e.g via SCP)

    scp *.tar.gz www@heytonton2:/opt/carto_json_data_splitted/

Extract them

    tar -xzf site0.tar.gz
    tar -xzf site1.tar.gz


Then import the batches of data (notice the `skipClear` argument to prevent collection purge):

    env=prod node src/import-json ../CARTO_DATA/json-splitted/site.0.json Site
    env=prod node src/import-json ../CARTO_DATA/json-splitted/site.1.json Site skipClear
    env=prod node src/import-json ../CARTO_DATA/json-splitted/site.2.json Site skipClear
    ...

---

### Export json data from existing Mongo instance

    mongodump --collection=communes --db=carto --out=.
    mongodump --collection=etats --db=carto --out=.
    mongodump --collection=sites --db=carto --out=.
    mongodump --collection=remarques --db=carto --out=.

---

### Import mongo dump (bson)

    mongorestore --db=carto communes.bson
    mongorestore --db=carto etats.bson
    mongorestore --db=carto sites.bson
    mongorestore --db=carto remarques.bson

---

### Configure properties

Edit and update config json files.

---

### Install and configure Apache2

    sudo apt-get install apache2

    cd /etc/apache2
    vim conf-available/charset.conf

Uncomment the line: AddDefaultCharset UTF-8

Set the virtual hosts and the hell of configuration of proxypass and https.
See ./vhosts folder

First, create regular vhost for http access

- heytonton.fr.conf
- carto.heytonton.fr.conf
- vadrouilles.fr.conf

Then enable https using `certbot` for these 3, enabling automatic redirection from http to https.

Finally vhosts from http acces can be disabled.

In the end only the ssl vhosts are enabled:

```
root@heytonton2:/etc/apache2# ll sites-enabled/
drwxr-xr-x 2 root root 4096 Aug  9 17:36 ./
drwxr-xr-x 9 root root 4096 Aug  9 17:38 ../
lrwxrwxrwx 1 root root   59 Aug  9 13:03 carto.heytonton.fr-le-ssl.conf -> /etc/apache2/sites-available/carto.heytonton.fr-le-ssl.conf
lrwxrwxrwx 1 root root   53 Aug  9 13:02 heytonton.fr-le-ssl.conf -> /etc/apache2/sites-available/heytonton.fr-le-ssl.conf
lrwxrwxrwx 1 root root   45 Aug  9 12:50 vadrouilles.fr-le-ssl.conf -> ../sites-available/vadrouilles.fr-le-ssl.conf
```

---

### Deploy project carto

Go to carto_client repo, check constants defined in configuration and deploy script, then depending on the target environement execute one of these:

- npm run deploy-inte
- npm run deploy-prod

---
