#!/bin/sh

DATE_LABEL=$(date +"%Y_%m_%d-%Hh%Mm%S")
DB_INSTANCE=carto
DUMPS_DIR=/opt/BACKUPS/carto
DUMP_NAME=$DATE_LABEL"_carto_prod_db"
ARCHIVE_NAME=$DUMP_NAME.tar.gz

mongodump --db=$DB_INSTANCE --out=$DUMPS_DIR/$DUMP_NAME && \

cd $DUMPS_DIR && \

tar czf $ARCHIVE_NAME $DUMP_NAME && \

echo
echo Archive available:
echo "${DUMPS_DIR}/${ARCHIVE_NAME}"

echo
rm -Rf $DUMPS_DIR/$DUMP_NAME
echo Cleared temporary folder

cd - > /dev/null
echo Done


