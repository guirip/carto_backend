
# Geo JSON Migration Guide

This documentation describes the operations needed to be able to query sites using their coordinates.

_e.g:_ finding sites matching some criteria and being in a 20km radius of a position

## Migrating the existing mongo database

### 1 - Remove sites without any position

First, mongo won't be able to create the `2dsphere` index if some sites don't have any position _(or maybe avoid setting null values in the coordinates array? see step 2)_.

As the database is used to display sites on a map, sites without a position are useless anyway.

> db.sites.deleteMany({ X_WGS_84: null })

### 2 - Feed the GEO_POINT field

> db.sites.updateMany({}, [{ $set: { GEO_POINT: { type: 'Point', coordinates: [ "$X_WGS_84", "$Y_WGS_84" ]}}}])

NB: longitude comes first in the array, and latitude is second.

### 3 - Create the index

> db.sites.createIndex({ GEO_POINT: '2dsphere' })

There should be no error displayed.

## Conclusion

That's it, you can now use the feature!

## Clean up

After a while, the `X_WGS_84` and `Y_WGS_84` fields should be deleted from the collection.
