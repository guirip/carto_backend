
# Departement Migration Guide

This documentation describes the operations needed to normalize departement management.

## Support departement value as string

> db.sites.updateMany({}, [{ $set: { DEPARTEMENT: { $toString: "$DEPARTEMENT" }}}])

> db.sites.updateMany({DEPARTEMENT: '0' }, [{ $set: { DEPARTEMENT: '00' }}])
> db.sites.updateMany({DEPARTEMENT: '1' }, [{ $set: { DEPARTEMENT: '01' }}])
> db.sites.updateMany({DEPARTEMENT: '2' }, [{ $set: { DEPARTEMENT: '02' }}])
> db.sites.updateMany({DEPARTEMENT: '3' }, [{ $set: { DEPARTEMENT: '03' }}])
> db.sites.updateMany({DEPARTEMENT: '4' }, [{ $set: { DEPARTEMENT: '04' }}])
> db.sites.updateMany({DEPARTEMENT: '5' }, [{ $set: { DEPARTEMENT: '05' }}])
> db.sites.updateMany({DEPARTEMENT: '6' }, [{ $set: { DEPARTEMENT: '06' }}])
> db.sites.updateMany({DEPARTEMENT: '7' }, [{ $set: { DEPARTEMENT: '07' }}])
> db.sites.updateMany({DEPARTEMENT: '8' }, [{ $set: { DEPARTEMENT: '08' }}])
> db.sites.updateMany({DEPARTEMENT: '9' }, [{ $set: { DEPARTEMENT: '09' }}])
