
# For diary

## Creation author

> db.sites.find({ CREATION_AUTHOR: { $ne: null } }).sort({ CREATION_DATE: -1 }).limit(80).explain('executionStats')

{
    "queryPlanner" : {
        "plannerVersion" : 1,
        "namespace" : "carto.sites",
        "indexFilterSet" : false,
        "parsedQuery" : {
            "CREATION_AUTHOR" : {
                "$not" : {
                    "$eq" : null
                }
            }
        },
        "winningPlan" : {
            "stage" : "SORT",
            "sortPattern" : {
                "CREATION_DATE" : -1
            },
            "limitAmount" : 80,
            "inputStage" : {
                "stage" : "SORT_KEY_GENERATOR",
                "inputStage" : {
                    "stage" : "COLLSCAN",
                    "filter" : {
                        "CREATION_AUTHOR" : {
                            "$not" : {
                                "$eq" : null
                            }
                        }
                    },
                    "direction" : "forward"
                }
            }
        },
        "rejectedPlans" : [ ]
    },
    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 80,                     <--- nb de résultats
        "executionTimeMillis" : 163,
        "totalKeysExamined" : 0,              <--- nb de documents parcourus à l'aide d'un index
        "totalDocsExamined" : 131009,         <--- nb de documents qu'il a fallu scanné l'un après l'autre
        "executionStages" : {
            "stage" : "SORT",
            "nReturned" : 80,
            "executionTimeMillisEstimate" : 9,
            "works" : 131093,
            "advanced" : 80,
            "needTime" : 131012,
            "needYield" : 0,
            "saveState" : 1024,
            "restoreState" : 1024,
            "isEOF" : 1,
            "sortPattern" : {
                "CREATION_DATE" : -1
            },
            "memUsage" : 57940,
            "memLimit" : 33554432,
            "limitAmount" : 80,
            "inputStage" : {
                "stage" : "SORT_KEY_GENERATOR",
                "nReturned" : 3049,
                "executionTimeMillisEstimate" : 9,
                "works" : 131012,
                "advanced" : 3049,
                "needTime" : 127962,
                "needYield" : 0,
                "saveState" : 1024,
                "restoreState" : 1024,
                "isEOF" : 1,
                "inputStage" : {
                    "stage" : "COLLSCAN",
                    "filter" : {
                        "CREATION_AUTHOR" : {
                            "$not" : {
                                "$eq" : null
                            }
                        }
                    },
                    "nReturned" : 3049,
                    "executionTimeMillisEstimate" : 9,
                    "works" : 131011,
                    "advanced" : 3049,
                    "needTime" : 127961,
                    "needYield" : 0,
                    "saveState" : 1024,
                    "restoreState" : 1024,
                    "isEOF" : 1,
                    "direction" : "forward",
                    "docsExamined" : 131009
                }
            }
        }
    },
    "serverInfo" : {
        "host" : "zenbook-pro",
        "port" : 27017,
        "version" : "4.2.8",
        "gitVersion" : "43d25964249164d76d5e04dd6cf38f6111e21f5f"
    },
    "ok" : 1
}


### Avec index

> db.sites.createIndex({ CREATION_AUTHOR: -1 })
{
    "createdCollectionAutomatically" : false,
    "numIndexesBefore" : 7,
    "numIndexesAfter" : 8,
    "ok" : 1
}

> db.sites.find({ CREATION_AUTHOR: { $ne: null } }).sort({ CREATION_DATE: -1 }).limit(80).explain('executionStats')
{
    "queryPlanner" : {
        "plannerVersion" : 1,
        "namespace" : "carto.sites",
        "indexFilterSet" : false,
        "parsedQuery" : {
            "CREATION_AUTHOR" : {
                "$not" : {
                    "$eq" : null
                }
            }
        },
        "winningPlan" : {
            "stage" : "SORT",
            "sortPattern" : {
                "CREATION_DATE" : -1
            },
            "limitAmount" : 80,
            "inputStage" : {
                "stage" : "SORT_KEY_GENERATOR",
                "inputStage" : {
                    "stage" : "FETCH",
                    "inputStage" : {
                        "stage" : "IXSCAN",
                        "keyPattern" : {
                            "CREATION_AUTHOR" : -1
                        },
                        "indexName" : "CREATION_AUTHOR_-1",
                        "isMultiKey" : false,
                        "multiKeyPaths" : {
                            "CREATION_AUTHOR" : [ ]
                        },
                        "isUnique" : false,
                        "isSparse" : false,
                        "isPartial" : false,
                        "indexVersion" : 2,
                        "direction" : "forward",
                        "indexBounds" : {
                            "CREATION_AUTHOR" : [
                                "[MaxKey, null)",
                                "(undefined, MinKey]"
                            ]
                        }
                    }
                }
            }
        },
        "rejectedPlans" : [ ]
    },
    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 80,
        "executionTimeMillis" : 22,       <---- 163ms sans index
        "totalKeysExamined" : 3050,       <----
        "totalDocsExamined" : 3049,       <----
        "executionStages" : {
            "stage" : "SORT",
            "nReturned" : 80,
            "executionTimeMillisEstimate" : 1,
            "works" : 3133,
            "advanced" : 80,
            "needTime" : 3052,
            "needYield" : 0,
            "saveState" : 24,
            "restoreState" : 24,
            "isEOF" : 1,
            "sortPattern" : {
                "CREATION_DATE" : -1
            },
            "memUsage" : 57940,
            "memLimit" : 33554432,
            "limitAmount" : 80,
            "inputStage" : {
                "stage" : "SORT_KEY_GENERATOR",
                "nReturned" : 3049,
                "executionTimeMillisEstimate" : 1,
                "works" : 3052,
                "advanced" : 3049,
                "needTime" : 2,
                "needYield" : 0,
                "saveState" : 24,
                "restoreState" : 24,
                "isEOF" : 1,
                "inputStage" : {
                    "stage" : "FETCH",
                    "nReturned" : 3049,
                    "executionTimeMillisEstimate" : 1,
                    "works" : 3051,
                    "advanced" : 3049,
                    "needTime" : 1,
                    "needYield" : 0,
                    "saveState" : 24,
                    "restoreState" : 24,
                    "isEOF" : 1,
                    "docsExamined" : 3049,
                    "alreadyHasObj" : 0,
                    "inputStage" : {
                        "stage" : "IXSCAN",
                        "nReturned" : 3049,
                        "executionTimeMillisEstimate" : 0,
                        "works" : 3051,
                        "advanced" : 3049,
                        "needTime" : 1,
                        "needYield" : 0,
                        "saveState" : 24,
                        "restoreState" : 24,
                        "isEOF" : 1,
                        "keyPattern" : {
                            "CREATION_AUTHOR" : -1
                        },
                        "indexName" : "CREATION_AUTHOR_-1",
                        "isMultiKey" : false,
                        "multiKeyPaths" : {
                            "CREATION_AUTHOR" : [ ]
                        },
                        "isUnique" : false,
                        "isSparse" : false,
                        "isPartial" : false,
                        "indexVersion" : 2,
                        "direction" : "forward",
                        "indexBounds" : {
                            "CREATION_AUTHOR" : [
                                "[MaxKey, null)",
                                "(undefined, MinKey]"
                            ]
                        },
                        "keysExamined" : 3050,
                        "seeks" : 2,
                        "dupsTested" : 0,
                        "dupsDropped" : 0
                    }
                }
            }
        }
    },
    "serverInfo" : {
        "host" : "zenbook-pro",
        "port" : 27017,
        "version" : "4.2.8",
        "gitVersion" : "43d25964249164d76d5e04dd6cf38f6111e21f5f"
    },
    "ok" : 1
}


## + index sur CREATION_DATE

> db.sites.createIndex({ CREATION_DATE: -1 })

    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 80,
        "executionTimeMillis" : 3,
        "totalKeysExamined" : 80,
        "totalDocsExamined" : 80,




## Position author

> db.sites.find({ POSITION_AUTHOR: { $ne: null } }).sort({ POSITION_DATE: -1 }).limit(80).explain('executionStats')


    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 80,
        "executionTimeMillis" : 212,
        "totalKeysExamined" : 0,
        "totalDocsExamined" : 131009,

### Avec index sur POSITION_AUTHOR

> db.sites.createIndex({ POSITION_AUTHOR: -1 })

explain:

    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 80,
        "executionTimeMillis" : 8,   <--- 212 -> 8 !
        "totalKeysExamined" : 510,
        "totalDocsExamined" : 509,


### + index sur POSITION_DATE

> db.sites.createIndex({ POSITION_DATE: -1 })

    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 80,
        "executionTimeMillis" : 2,
        "totalKeysExamined" : 80,
        "totalDocsExamined" : 80,





## Etat author

> db.sites.find({ ETAT_AUTHOR: { $ne: null } }).sort({ ETAT_DATE: -1 }).limit(80).explain('executionStats')
{
    "queryPlanner" : {
        "plannerVersion" : 1,
        "namespace" : "carto.sites",
        "indexFilterSet" : false,
        "parsedQuery" : {
            "ETAT_AUTHOR" : {
                "$not" : {
                    "$eq" : null
                }
            }
        },
        "winningPlan" : {
            "stage" : "SORT",
            "sortPattern" : {
                "ETAT_DATE" : -1
            },
            "limitAmount" : 80,
            "inputStage" : {
                "stage" : "SORT_KEY_GENERATOR",
                "inputStage" : {
                    "stage" : "COLLSCAN",
                    "filter" : {
                        "ETAT_AUTHOR" : {
                            "$not" : {
                                "$eq" : null
                            }
                        }
                    },
                    "direction" : "forward"
                }
            }
        },
        "rejectedPlans" : [ ]
    },
    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 80,
        "executionTimeMillis" : 210,
        "totalKeysExamined" : 0,
        "totalDocsExamined" : 131009,



### Avec index:

> db.sites.createIndex({ ETAT_AUTHOR: -1 })

    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 80,
        "executionTimeMillis" : 60,
        "totalKeysExamined" : 6217,
        "totalDocsExamined" : 6216,


### + index sur ETAT_DATE

> db.sites.createIndex({ ETAT_DATE: -1 })

    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 80,
        "executionTimeMillis" : 4,
        "totalKeysExamined" : 80,
        "totalDocsExamined" : 80,


## Filtre

> db.sites.find({ DEPARTEMENT: { $in: [ 77, 91, 92, 93 ] },  ETAT: { $in: [ 3, 4, 5, 6, 7, 8 ] },  IS_ACTIVITE_TERMINEE: { $ne: 0 },  SITE_EN_FRICHE: { $ne: 0 },  SITE_REAMENAGE: { $ne: 1 },  X_WGS_84: { $ne: null },  Y_WGS_84: { $ne: null },  }).explain('executionStats')



  "executionStats" : {
    "executionSuccess" : true,
    "nReturned" : 492,
    "executionTimeMillis" : 59,
    "totalKeysExamined" : 18602,
    "totalDocsExamined" : 18601,


> db.sites.createIndex({ DEPARTEMENT: 1 })
> db.sites.createIndex({ ETAT: 1 })

db.sites.createIndex({ IS_ACTIVITE_TERMINEE: -1 })
db.sites.createIndex({ SITE_EN_FRICHE: -1 })
db.sites.createIndex({ SITE_REAMENAGE: -1 })
db.sites.createIndex({ X_WGS_84: -1 })
db.sites.createIndex({ Y_WGS_84: -1 })


    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 492,
        "executionTimeMillis" : 39,
        "totalKeysExamined" : 8498,
        "totalDocsExamined" : 8496,


en supprimant les indexes sur IS_ACTIVITE_TERMINEE, SITE_EN_FRICHE, SITE_REAMENAGE, X_WGS_84, Y_WGS_84

    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 492,
        "executionTimeMillis" : 21,
        "totalKeysExamined" : 8498,
        "totalDocsExamined" : 8496,
