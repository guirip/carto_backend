

db.remarques.find({}).sort({ DATE: -1 }).limit(80).explain('executionStats')

Sans index:

    "executionStats" : {
        "executionSuccess" : true,
        "nReturned" : 80,
        "executionTimeMillis" : 0,
        "totalKeysExamined" : 80,
        "totalDocsExamined" : 80,

Ajouter un index ne semble pas nécessaire à l'heure actuelle