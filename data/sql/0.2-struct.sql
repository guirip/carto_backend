
-- Listing des photos uploadees

CREATE TABLE PHOTOS (
    ID MEDIUMINT UNSIGNED AUTO_INCREMENT,
    FILENAME VARCHAR(255),
    IMPORT_DATE DATETIME,
    IMPORT_AUTHOR VARCHAR(30),
    SITE_ID INT UNSIGNED,
    COMMENTAIRE VARCHAR(255),
    PRIMARY KEY (ID),
    INDEX (SITE_ID)
);
