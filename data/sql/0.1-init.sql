
-- Install mysql server 5.6 :
-- sudo apt-get install mysql-server-5.6

-- Log in as root :
-- mysql -u root -p

-- Create user carto :
create user 'carto'@'%' identified by 'passmo2!carto';

-- Create database CARTO :
create database CARTO default character set = 'utf8';
use CARTO;

-- Give basic privileges to user carto :
grant select,insert,update,delete on CARTO.* to 'carto'@'%';

-- Create main table :

CREATE TABLE SITES (
    ID MEDIUMINT UNSIGNED AUTO_INCREMENT,
    CREATION_DATE DATETIME,
    CREATION_AUTHOR VARCHAR(30),
	IDENTIFIANT VARCHAR(10),
	CODE_INSEE MEDIUMINT UNSIGNED,
	DEPARTEMENT TINYINT,
	RAISON_SOCIALE VARCHAR(255),
	NOM_USUEL VARCHAR(255),
	COMMENTAIRE VARCHAR(255),
	ADRESSE VARCHAR(120),
	COMMENTAIRE_LOCALISATION VARCHAR(255),
	ETAT_DE_CONNAISSANCE VARCHAR(100),
	ETAT_OCCUPATION VARCHAR(100),
	IS_ACTIVITE_TERMINEE TINYINT,
	CODE_ACTIVITE VARCHAR(100),
	LIBELLE_ACTIVITE VARCHAR(500),
	COMMENTAIRE_ACTIVITE VARCHAR(500),
	X_LAMBERT_2E INT UNSIGNED,
	Y_LAMBERT_2E INT UNSIGNED,
	X_LAMBERT_93 INT UNSIGNED,
	Y_LAMBERT_93 INT UNSIGNED,
	PRECISION_XY VARCHAR(100),
	X_ADRESSE_LAMBERT_2E INT UNSIGNED,
	Y_ADRESSE_LAMBERT_2E INT UNSIGNED,
	PRECISION_ADRESSE VARCHAR(100),
	SURFACE_TOTALE FLOAT,
	SITE_REAMENAGE TINYINT UNSIGNED,
	SITE_EN_FRICHE TINYINT UNSIGNED,
	TYPE_DE_REAMENAGEMENT VARCHAR(255),
	PROJET_DE_REAMENAGEMENT VARCHAR(255),
	X_WGS_84 FLOAT,
	Y_WGS_84 FLOAT,
	POSITION_AUTHOR VARCHAR(30),
	POSITION_DATE DATETIME,
	ETAT TINYINT UNSIGNED,
	ETAT_AUTHOR VARCHAR(30),
	ETAT_DATE DATETIME,
	PRIMARY KEY (ID),
	UNIQUE KEY (IDENTIFIANT),
	INDEX (CODE_INSEE),
	INDEX (ETAT_OCCUPATION),
	INDEX (IS_ACTIVITE_TERMINEE),
	INDEX (SITE_REAMENAGE),
	INDEX (SITE_EN_FRICHE),
	INDEX (ETAT)
);

-- Create table COMMUNES :

CREATE TABLE COMMUNES (
    ID SMALLINT UNSIGNED AUTO_INCREMENT,
	COMMUNE VARCHAR(100),
	CP MEDIUMINT UNSIGNED,
	INSEE MEDIUMINT UNSIGNED,
	PRIMARY KEY (ID),
	UNIQUE KEY (INSEE),
	INDEX (INSEE)
);

-- Import communes :
-- mysql -u carto -p < insee/insee.sql

-- La requête suivante permet de vérifier que tous les sites ont leur correspondance dans la table COMMUNES :
-- select s.ID, s.CODE_INSEE from SITES s where not exists (select 1 from COMMUNES c where s.CODE_INSEE = c.INSEE);



-- Create table ETATS :

CREATE TABLE ETATS (
    ID SMALLINT UNSIGNED,
    LIBELLE VARCHAR(50),
    PRIMARY KEY (ID)
);

INSERT INTO ETATS (ID, LIBELLE) VALUES
(0, 'Rasé / Réhabilité'),
(1, 'N''a pas l''air abandonné'),
(2, 'Pas trouvé'),
(3, 'Terrain pas terrible'),
(4, 'Impossible de se prononcer'),
(5, 'Non vérifié'),
(6, 'Terrain confirmé mais...'),
(7, 'A aller voir à l''occasion...'),
(8, 'A aller voir rapidement !'),
(9, 'Terrain sympa'),
(10, 'Royaume');


-- Create table REMARQUES :

CREATE TABLE REMARQUES (
    ID SMALLINT UNSIGNED AUTO_INCREMENT,
    SITE_ID INT UNSIGNED,
	AUTHOR VARCHAR(30),
	DATE DATETIME,
	REMARQUE VARCHAR(500),
    PRIMARY KEY (ID),
	INDEX (SITE_ID)
);




-- SITES RESWA

insert into SITES
  (CODE_INSEE,DEPARTEMENT,RAISON_SOCIALE,ADRESSE,ETAT_OCCUPATION,IS_ACTIVITE_TERMINEE,SITE_EN_FRICHE,SITE_REAMENAGE,X_WGS_84,Y_WGS_84,ETAT,ETAT_AUTHOR,ETAT_DATE)
 values
  ((select INSEE from COMMUNES where COMMUNE = 'Chaux des Crotenay' and CP between 39000 and 39999),39,'Scierie','13 N5','Activité terminée',1,1,0,5.9427886917876,46.65151938985,7,'reswa',now());

insert into SITES
  (CODE_INSEE,DEPARTEMENT,RAISON_SOCIALE,ADRESSE,ETAT_OCCUPATION,IS_ACTIVITE_TERMINEE,SITE_EN_FRICHE,SITE_REAMENAGE,X_WGS_84,Y_WGS_84,ETAT,ETAT_AUTHOR,ETAT_DATE)
 values
  ((select INSEE from COMMUNES where COMMUNE = 'Merceuil' and CP between 21000 and 21999),21,'Archéodrome de Beaune','Autoroute du Soleil','Activité terminée',1,1,0,4.8338212733459,46.963415953341,7,'reswa',now());

insert into SITES
  (CODE_INSEE,DEPARTEMENT,RAISON_SOCIALE,ADRESSE,ETAT_OCCUPATION,IS_ACTIVITE_TERMINEE,SITE_EN_FRICHE,SITE_REAMENAGE,X_WGS_84,Y_WGS_84,ETAT,ETAT_AUTHOR,ETAT_DATE)
 values
  ((select INSEE from COMMUNES where COMMUNE = 'Morez' and CP between 39000 and 39999),39,'?','32 Rue Wladimir Gagneur','Activité terminée',1,1,0,6.026356904068,46.514085802509,7,'reswa',now());

insert into SITES
  (CODE_INSEE,DEPARTEMENT,RAISON_SOCIALE,ADRESSE,ETAT_OCCUPATION,IS_ACTIVITE_TERMINEE,SITE_EN_FRICHE,SITE_REAMENAGE,X_WGS_84,Y_WGS_84,ETAT,ETAT_AUTHOR,ETAT_DATE)
 values
  ((select INSEE from COMMUNES where COMMUNE = 'St Laurent en Grandvaux' and CP between 39000 and 39999),39,'?','4 Rue du 3ème Spahis','Activité terminée',1,1,0,5.9517901852416,46.578272729473,7,'reswa',now());


